<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:-2px;left:68px;height:auto;"
    onModelConstruct="modelModelConstruct"/>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-content" xid="content1"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel2"> 
        <div class="x-panel-top tops" xid="top1" style="width:100%;height:12rem;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row1"> 
            <div class="x-col" xid="col1"> 
              <a component="$UI/system/components/justep/button/button" label=""
                class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
                xid="backBtn"> 
                <i class="icon-chevron-left"/>  
                <span/> 
              </a> 
            </div>  
            <div class="x-col" xid="col2"> 
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row7"> 
                <div class="x-col x-col-fixed" xid="col19" style="width:auto;"/>  
                <div class="x-col" xid="col20" style="width:80px;height:80px;border-radius:40px;"> 
                  <img src="$UI/longrange/images/timg.jpg" alt="" xid="image1"
                    style="width:100%;height:100%;border-radius:40px;"/> 
                </div>  
                <div class="x-col x-col-fixed x-col-center" xid="col21" style="width:auto;"/> 
              </div>  
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row2"> 
                <div class="x-col name" xid="col5" style="color:#fff;"/> 
              </div> 
            </div>  
            <div class="x-col" xid="col3"> 
              <div class="x-col gggg bai" xid="col16" style="margin-top:2rem;">ᠮᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ</div> 
            </div> 
          </div> 
        </div>  
        <div class="x-panel-content bigbg" xid="content2" style="overflow-x:scroll;max-height:50.5rem;width:auto;top:12rem;"> 
        
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row3" style="height:auto;width:auto;overflow:scroll;"> 
               <div class="x-col x-col-10" xid="col4" style="background:#f8f6f7;padding:0px;height:33rem;"> 
              <div component="$UI/system/components/justep/row/row" class="x-row xm"
                xid="row4" style="border-right:2px solid #2fa4e7;border-bottom:1px #ddd solid;"
                bind-click="row4Click"> 
                <div class="x-col gggg" xid="col12" style="height:10rem;text-align:center;padding:0px">1.ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ  ᠢᠶᠠᠨ ᠲᠡᠭᠦᠯᠳᠡᠷᠵᠢᠭᠦᠯᠭᠦ</div> 
              </div>  
              <div component="$UI/system/components/justep/row/row" class="x-row xh"
                xid="row5" bind-click="row5Click" style="padding:0px;margin-top:2px;"> 
                <div class="x-col gggg" xid="col11" style="height:10rem;text-align:center;">2.完善资料</div> 
              </div> 
              <div component="$UI/system/components/justep/row/row" class="x-row xhz"
                xid="row44" bind-click="row44Click" style="padding:0px;margin-top:2px;"> 
                <div class="x-col gggg" xid="col11" style="height:10rem;text-align:center;">3.ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠦ 证件</div> 
              </div> 
            </div> 
            <div class="x-col" xid="col7" style="min-height:48rem;"> 
              <!-- 蒙语完善资料 -->  
              <div class="mxc"> 
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row16"> 
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit1"> 
                    <label class="x-label" xid="label1">ᠨᠡᠷ᠎ᠡ:</label>  
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input5"
                      style="margin-top:4rem;"/> 
                  </div>  
                
                 
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit3" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label3">ᠤᠲᠠᠰᠤᠨ  ᠲᠤᠭᠠᠷ:</label>
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input6" style="margin-top:1.1rem;"/>
                  </div>
              
               
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit9" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label9">ᠭᠠᠵᠠᠷ  ᠤᠷᠤᠨᠯ:</label>  
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input8" style="margin-top:1.6rem;"/>
                  </div>
                
                </div> 
              </div>  
              <!-- 汉语资料 -->  
              <div class="hxc" style="display:none"> 
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row6"> 
                  <div class="x-col x-col-20" xid="col6">姓名</div>  
                  <div class="x-col" xid="col9"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input1"/> 
                  </div> 
                </div>  
               
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row9"> 
                  <div class="x-col x-col-20" xid="col15">手机号</div>  
                  <div class="x-col" xid="col17"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input2"/> 
                  </div> 
                </div>  
             
                
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row14"> 
                  <div class="x-col x-col-20" xid="col34">地区</div>  
                  <div class="x-col" xid="col35"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input4"/> 
                  </div> 
                </div>  
               
              </div>  
              <!-- 结束资料 --> 
               <!-- 证件资料 -->  
          <div class="hxcz" style="display:none"> 
                   <div class="x-col" xid="col7" style="border-left:1px #ddd solid;background:#fff;">
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row6"> 
                <div class="x-col x-col-fixed x-col-center" xid="col14" style="width:auto;">
                  <a component="$UI/system/components/justep/button/button"
                    class="btn btn-default btn-only-icon" label="button" xid="button1"
                    icon="linear linear-bus"> 
                    <i xid="i1" class="linear linear-bus"/>  
                    <span xid="span1"/>
                  </a>
                </div>  
                <div class="x-col" xid="col15">
                     <div style="text-align:center;margin-top:2.5rem;color:#ddd;">您还没上传图片</div>
                  <!-- <img src="" alt="" xid="image2"/> -->
                </div> 
              </div>
            </div>
              </div>  
              <!-- 证件资料 --> 
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
  </div> 
</div>
