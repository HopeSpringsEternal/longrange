define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function(){
		this.callParent();
	};

	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
	};
	Model.prototype.li1Click = function(event){
      justep.Shell.showPage("ygrenwu");
	};
	Model.prototype.li2Click = function(event){
      justep.Shell.showPage("ygyaofang");
	};
	Model.prototype.li6Click = function(event){
      justep.Shell.showPage("ygrenzheng"); 
	};
	Model.prototype.li5Click = function(event){
      justep.Shell.showPage("ygwanshan");
	};
	Model.prototype.li4Click = function(event){
      justep.Shell.showPage("ygyaopin");
	};

	return Model;
});