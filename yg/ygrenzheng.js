define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function(){
		this.callParent();
	};

	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
	};
	Model.prototype.li1Click = function(event){
      justep.Shell.showPage("zgindex");
	};
	Model.prototype.li2Click = function(event){
      justep.Shell.showPage("zgzhenguan");
	};
	return Model;
});