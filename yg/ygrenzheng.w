<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="left:18px;top:83px;height:244px;"
    onModelConstruct="modelModelConstruct"/>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-content" xid="content1"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel2"> 
        <div class="x-panel-top tops" xid="top1" style="width:100%;height:12rem;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row1"> 
            <div class="x-col" xid="col1"> 
              <a component="$UI/system/components/justep/button/button" label=""
                class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
                xid="backBtn"> 
                <i class="icon-chevron-left"/>  
                <span/> 
              </a> 
            </div>  
            <div class="x-col" xid="col2"> 
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row7"> 
                <div class="x-col x-col-fixed" xid="col19" style="width:auto;"/>  
                <div class="x-col" xid="col20" style="width:80px;height:80px;border-radius:40px;"> 
                  <img src="$UI/longrange/images/timg.jpg" alt="" xid="image1"
                    style="width:100%;height:100%;border-radius:40px;"/> 
                </div>  
                <div class="x-col x-col-fixed x-col-center" xid="col21" style="width:auto;"/> 
              </div>  
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row2"> 
                <div class="x-col name" xid="col5" style="color:#fff;"/> 
              </div>
            </div>  
            <div class="x-col" xid="col3"> 
              <div class="x-col gggg bai" xid="col16" style="margin-top:2rem;">ᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ</div> 
            </div> 
          </div> 
        </div>  
        <div class="x-panel-content bigbg" xid="content2" style="overflow-x:scroll;max-height:48.5rem;width:auto;top:12rem;"> 
         
         <!-- 资格 -->
           <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row3" style="background:#fff;margin-top:2px;"> 
            <div class="x-col x-col-20" xid="col4" >
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row4"> 
                <div class="x-col gggg" xid="col8">ᠬᠢᠷᠢ ᠬᠡᠮ ᠦᠨ ᠦᠨᠡᠮᠯᠡᠬᠦ</div>
              </div>  
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row5"> 
                <div class="x-col" xid="col11">资格证书</div>
              </div>
            </div>  
            <div class="x-col" xid="col7" style="border-left:1px #ddd solid;">
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row6"> 
                <div class="x-col x-col-fixed x-col-center" xid="col14" style="width:auto;">
                  <a component="$UI/system/components/justep/button/button"
                    class="btn btn-default btn-only-icon" label="button" xid="button1"
                    icon="linear linear-bus"> 
                    <i xid="i1" class="linear linear-bus"/>  
                    <span xid="span1"/>
                  </a>
                </div>  
                <div class="x-col" xid="col15">
                     <div style="text-align:center;margin-top:2.5rem;color:#ddd;">您还没上传图片</div>
                  <!-- <img src="" alt="" xid="image2"/> -->
                </div> 
              </div>
            </div>
          </div>
          <!-- 结束 -->
        </div> 
      </div> 
    </div> 
  </div> 
</div>
