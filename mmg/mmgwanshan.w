<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:-2px;left:68px;height:auto;"
    onModelConstruct="modelModelConstruct"/>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-content" xid="content1"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel2"> 
        <div class="x-panel-top tops" xid="top1" style="width:100%;height:12rem;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row1"> 
            <div class="x-col" xid="col1"> 
              <a component="$UI/system/components/justep/button/button" label=""
                class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
                xid="backBtn"> 
                <i class="icon-chevron-left"/>  
                <span/> 
              </a> 
            </div>  
            <div class="x-col" xid="col2"> 
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row7"> 
                <div class="x-col x-col-fixed" xid="col19" style="width:auto;"/>  
                <div class="x-col" xid="col20" style="width:80px;height:80px;border-radius:40px;"> 
                  <img src="$UI/longrange/images/timg.jpg" alt="" xid="image1"
                    style="width:100%;height:100%;border-radius:40px;"/> 
                </div>  
                <div class="x-col x-col-fixed x-col-center" xid="col21" style="width:auto;"/> 
              </div>  
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row2"> 
                <div class="x-col name" xid="col5" style="color:#fff;"/> 
              </div> 
            </div>  
            <div class="x-col" xid="col3"> 
              <div class="x-col gggg bai" xid="col16" style="margin-top:2rem;">ᠮᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ</div> 
            </div> 
          </div> 
        </div>  
        <div class="x-panel-content bigbg" xid="content2" style="overflow-x:scroll;max-height:50.5rem;width:auto;top:12rem;"> 
        
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row3" style="height:auto;width:auto;overflow:scroll;"> 
            <div class="x-col x-col-10" xid="col4" style="background:#f8f6f7;padding:0px;height:9rem;"> 
              <div component="$UI/system/components/justep/row/row" class="x-row xm"
                xid="row4" style="border-right:2px solid #2fa4e7;border-bottom:1px #ddd solid;"
                bind-click="row4Click"> 
                <div class="x-col gggg" xid="col12" style="height:10rem;text-align:center;padding:0px">ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ  ᠢᠶᠠᠨ ᠲᠡᠭᠦᠯᠳᠡᠷᠵᠢᠭᠦᠯᠭᠦ</div> 
              </div>  
              <div component="$UI/system/components/justep/row/row" class="x-row xh"
                xid="row5" bind-click="row5Click" style="padding:0px;margin-top:2px;"> 
                <div class="x-col gggg" xid="col11" style="height:10rem;text-align:center;">完善资料</div> 
              </div> 
            </div>  
            <div class="x-col" xid="col7" style="min-height:48rem;"> 
              <!-- 蒙语完善资料 -->  
              <div class="mxc"> 
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row16"> 
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit1"> 
                    <label class="x-label" xid="label1">ᠨᠡᠷ᠎ᠡ:</label>  
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input5"
                      style="margin-top:4rem;"/> 
                  </div>  
                
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit2" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label2">ᠴᠢᠨᠠᠷ  ᠤᠨ  ᠢᠯᠭᠠᠯ:</label>  
                    <span component="$UI/system/components/justep/select/radioGroup"
                      class="x-radios x-edit" xid="radioGroup2" style="margin-top:1rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit3" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label3">ᠤᠲᠠᠰᠤᠨ  ᠲᠤᠭᠠᠷ:</label>
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input6" style="margin-top:1.1rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit4" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label4">ᠰᠤᠶᠤᠯ  ᠤᠨ  ᠬᠡᠮᠵᠢᠶ᠎ᠡ:</label> 
                    <select component="$UI/system/components/justep/select/select"
                      class="form-control x-edit mgl-verticals-text" xid="select4" style="margin-top:0.5rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit6" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label6">ᠠᠩᠭᠢᠯᠠᠯ:</label>
                    <select component="$UI/system/components/justep/select/select"
                      class="form-control x-edit mgl-verticals-text" xid="select6" style="margin-top:3rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit7" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label7">ᠲᠤᠰᠬᠠᠢ  ᠮᠡᠷᠭᠡᠵᠢᠯ:</label>
                    <select component="$UI/system/components/justep/select/select"
                      class="form-control x-edit mgl-verticals-text" xid="select7" style="margin-top:1rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit8" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label8">ᠪᠡᠶ᠎ᠡ  ᠶᠢᠨ  ᠦᠨᠡᠮᠯᠡᠯ:</label> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input7" style="margin-top:0.4rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit9" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label9">ᠭᠠᠵᠠᠷ  ᠤᠷᠤᠨᠯ:</label>  
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control x-edit mgl-verticals-text" xid="input8" style="margin-top:1.6rem;"/>
                  </div>
                  <div component="$UI/system/components/justep/labelEdit/labelEdit"
                    class="x-label-edit x-label30 mgl-verticals" xid="labelEdit10" style="margin-left:1rem;"> 
                    <label class="x-label" xid="label10">ᠬᠤᠪᠢ  ᠶᠢᠨ  ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ :</label> 
                    <textarea component="$UI/system/components/justep/textarea/textarea"
                      class="form-control x-edit mgl-verticals-text" xid="textarea2"/>
                  </div>
                </div> 
              </div>  
              <!-- 汉语资料 -->  
              <div class="hxc" style="display:none"> 
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row6"> 
                  <div class="x-col x-col-20" xid="col6">姓名</div>  
                  <div class="x-col" xid="col9"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input1"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row8"> 
                  <div class="x-col x-col-20" xid="col10">性别</div>  
                  <div class="x-col" xid="col14"> 
                    <span component="$UI/system/components/justep/select/radioGroup"
                      class="x-radio-group" xid="radioGroup1"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row9"> 
                  <div class="x-col x-col-20" xid="col15">手机号</div>  
                  <div class="x-col" xid="col17"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input2"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row10"> 
                  <div class="x-col x-col-20" xid="col22">学历</div>  
                  <div class="x-col" xid="col23"> 
                    <select component="$UI/system/components/justep/select/select"
                      bind-optionsCaption="请选择..." class="form-control" xid="select1"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row11"> 
                  <div class="x-col x-col-20" xid="col25">分类</div>  
                  <div class="x-col" xid="col26"> 
                    <select component="$UI/system/components/justep/select/select"
                      bind-optionsCaption="请选择..." class="form-control" xid="select2"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row12"> 
                  <div class="x-col x-col-20" xid="col28">专业</div>  
                  <div class="x-col" xid="col29"> 
                    <select component="$UI/system/components/justep/select/select"
                      bind-optionsCaption="请选择..." class="form-control" xid="select3"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row13"> 
                  <div class="x-col x-col-20" xid="col31">身份证号</div>  
                  <div class="x-col" xid="col32"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input3"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row14"> 
                  <div class="x-col x-col-20" xid="col34">地区</div>  
                  <div class="x-col" xid="col35"> 
                    <input component="$UI/system/components/justep/input/input"
                      class="form-control" xid="input4"/> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row15"> 
                  <div class="x-col x-col-20" xid="col37">介绍</div>  
                  <div class="x-col" xid="col38"> 
                    <textarea component="$UI/system/components/justep/textarea/textarea"
                      class="form-control" xid="textarea1"/> 
                  </div> 
                </div> 
              </div>  
              <!-- 结束资料 --> 
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
  </div> 
</div>
