define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
    var zz=window.menghan;
	var Model = function(){
		this.callParent();
	};
  //选择默认选择页面
	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
      if(window.menghan == 2){
    
      $('.yxm').css("display","none");
      $('.yxh').css("display","block"); 
  
      $('.ymxc').css("display","none");
      $('.yhxc').css("display","block");
      }else{
      $('.yxm').css("display","block");
      $('.yxh').css("display","none"); 
  
      $('.ymxc').css("display","block");
      $('.yhxc').css("display","none");
      }
      
	};

	//转换已处理蒙语样子
	Model.prototype.row6Click = function(event){
	   zz=1;
     
      $('.yxm').css("border-right","2px solid #2fa4e7");
      $('.yxh').css("border-right","0px solid #2fa4e7"); 

      $('.ymxc').css("display","block");
      $('.yhxc').css("display","none");
	};
	//转换已处理蒙语样子
	Model.prototype.row7Click = function(event){
	   zz=2;

      $('.yxm').css("border-right","0px solid #2fa4e7");
      $('.yxh').css("border-right","2px solid #2fa4e7"); 

      $('.ymxc').css("display","none");
      $('.yhxc').css("display","block");
	};
	//查看蒙语诊断数据
	Model.prototype.li1Click = function(event){	  
     var row=event.bindingContext.$object;
    
       this.comp("yichulimdialog").open({
        params:{
              code:"m",
              rowData:row.toJson()
        }     
       });
	};
	//查看汉语诊断数据
	Model.prototype.li2Click = function(event){
      var row=event.bindingContext.$object;
    
       this.comp("yichulimdialog").open({
        params:{
              code:"z",
              rowData:row.toJson()
        }     
       });
	};



	Model.prototype.modelLoad = function(event){
     var count = this.comp('mzhenduan').count();
     var zong=5*count;
       $('.wmxc').css("width",zong+"rem");
       $('.wmxc').css("background","#fff");
	};
	return Model;
});