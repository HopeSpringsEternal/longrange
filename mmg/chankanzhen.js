define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function(){
	   
		this.callParent();
	};
  
    //新添加诊断表的时候蒙汉转换；
	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
      if(window.menghan == 2){
      $('.xm').css("border-right","0px solid #2fa4e7");
      $('.xh').css("border-right","2px solid #2fa4e7");
      $('.mxc').css("display","none");
      $('.hxc').css("display","block");
      }
      
	};

	//新添加诊断表的时候蒙汉转换；
	Model.prototype.row4Click = function(event){
      $('.xm').css("border-right","2px solid #2fa4e7");
      $('.xh').css("border-right","0px solid #2fa4e7"); 
      $('.mxc').css("display","block");
      $('.hxc').css("display","none");
	};
	//新添加诊断表的时候蒙汉转换；
	Model.prototype.row5Click = function(event){
	  $('.xm').css("border-right","0px solid #2fa4e7");
      $('.xh').css("border-right","2px solid #2fa4e7");
      $('.mxc').css("display","none");
      $('.hxc').css("display","block");
	};
    //接受数据
	Model.prototype.windowReceiver1Receive = function(event) {
	
		// 对话框接收参数后，新增或编辑
		var accountData = this.comp("mzhenduan");
		//var hqmumin = this.comp("hqmumin");
		accountData.clear();
	
		this.code = event.params.code;
	  
		//新增出现弹出框，先获取牧民信息
		
			accountData.loadData([event.params.rowData]);
			accountData.first();
			if(this.code == "m"){
			      $('.xm').css("display","block");
			      $('.xh').css("display","none");
			      $('.mxc').css("display","block");
			      $('.hxc').css("display","none");
			      $('.zh').css("height","11rem");
			}else{
			      $('.xm').css("display","none");
			      $('.xh').css("display","block");
			      $('.mxc').css("display","none");
			      $('.hxc').css("display","block");
			      $('.zh').css("height","11rem");
			}
		
	};
	//推荐药片
	Model.prototype.tuiyaobtnClick = function(event){
	   $('.yaoxianshi').css('display','block');
	  /*var tuiyao = this.comp("tuiyaodialog");
       tuiyao.open();*/
	};
	

	
	
	
	Model.prototype.button1Click = function(event){
       this.close();
	};
	
	Model.prototype.tuiyaohClick = function(event){
	 $('.yanpinhh').css('display','block');
          
	};
	
	return Model;
});