<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:pc;">  
  <div component="$UI/system/components/justep/model/model" xid="model" onLoad="modelLoad">
    <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="mumin" idColumn="id"><column name="id" type="String" xid="xid11"></column>
  <column name="name" type="String" xid="xid12"></column>
  <column name="img" type="String" xid="xid13"></column></div><div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="tuisong" idColumn="id"> 
      <column name="id" type="String" xid="xid1"/>  
      <column name="createTime" type="String" xid="xid2"/>  
      <column name="updateTime" type="String" xid="xid3"/>  
      <column name="hid" type="String" xid="xid4"/>  
      <column name="roomid" type="String" xid="xid5"/>  
      <column name="url" type="String" xid="xid6"/>  
      <column name="time" type="String" xid="xid7"/>  
      <column name="headurl" type="String" xid="xid8"/>  
      <column name="username" type="String" xid="xid9"/>  
      <column name="eid" type="String" xid="xid10"/>
    </div>
  </div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full dengbg"
    xid="panel1"> 
    <div class="x-panel-content" xid="content1" style="color:#000;"> 
       
      <div bind-text=" $model.tuisong.val(&quot;createTime&quot;)" style="display:none;"/>  
      <div bind-text=" $model.tuisong.val(&quot;updateTime&quot;)" style="display:none;"/>  
     
      <div class="hid" bind-text=" $model.tuisong.val(&quot;hid&quot;)" style="display:none;"/>  
      <div class="roomid" bind-text=" $model.tuisong.val(&quot;roomid&quot;)" style="display:none;"/>  
      <div class="url" bind-text=" $model.tuisong.val(&quot;url&quot;)" style="display:none;"/>  
      <div bind-text=" $model.tuisong.val(&quot;time&quot;)" style="display:none;"/>  
      <div bind-text=" $model.tuisong.val(&quot;headurl&quot;)" style="display:none;"/>  
      <div bind-text=" $model.tuisong.val(&quot;username&quot;)" style="display:none;"/>  
      <div style="display:none;"/>  
      <div style="display:none;"/>  
      <div style="display:none;"/> 
      <div class="media" xid="media1" style="position:absolute;left:30%;top:20%;">
   <div class="media-left" xid="mediaLeft1">
    <a href="#" xid="a1">
     <img class="media-object" src="" alt="" xid="image1" style="width:80px;height:80px;border-radius:40px;" bind-attr-src=' $model.mumin.val("img")'></img></a> </div> 
   <div class="media-body" xid="mediaBody1">
    <h4 class="media-heading gggg" style="color:#fff;font-size:30px;height:20rem;margin-left:2rem" xid="h41" bind-text='$model.mumin.val("name")'><![CDATA[]]></h4></div> </div>
    </div>  
    
    <div class="x-panel-bottom" xid="bottom1" >
      <a component="$UI/system/components/justep/button/button" class="btn x-red btn-lg btn-icon-top btn-only-icon"
         xid="cancelBtn" onClick="cancelBtnClick" icon="linear linear-phonehandset" style="position:absolute;left:60%;bottom:20%;width:80px;height:80px;border-radius:40px;"> 
        <i xid="i2" class="linear-phonehandset linear" />  
    
      </a>  
      <a component="$UI/system/components/justep/button/button" class="btn x-green btn-lg btn-icon-top btn-only-icon x-dialog-button"
        label="接受" xid="OKBtn" onClick="OKBtnClick" icon="linear linear-phonehandset" style="position:absolute;left:20%;bottom:20%;width:80px;height:80px;border-radius:40px;"> 
        <i xid="i1" class="linear linear-phonehandset"/>  
        <span xid="span1">接受</span>
      </a>
    </div>
  </div>  
  <span component="$UI/system/components/justep/windowReceiver/windowReceiver"
    xid="wReceiver" style="left:443px;top:568px;" onReceive="wReceiverReceive"/>
</div>
