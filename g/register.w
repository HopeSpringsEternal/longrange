<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window"
  design="device:m;">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;left:75px;top:9px;">
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="payData" idColumn="fID"> 
      <column name="fID" type="String" xid="default14"/>  
      <column label="支付方式" name="fPayType" type="String" xid="default8"/>  
      <column label="支付方式名字" name="fPayTypeLabel" type="String" xid="default29"/>  
      <data xid="default36">[{"fID":"1","fPayType":"4","fPayTypeLabel":"牧民"},{"fID":"2","fPayType":"5","fPayTypeLabel":"专家"},{"fID":"3","fPayType":"6","fPayTypeLabel":"药店"}]</data>
    </div>
  </div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-top" xid="top1" height="48"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" class="x-titlebar"
        xid="titleBar1" title="注册" style="height:100%;"> 
        <div class="x-titlebar-left"> 
          <a component="$UI/system/components/justep/button/button" label=""
            class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
            xid="backBtn"> 
            <i class="icon-chevron-left"/>  
            <span/> 
          </a> 
        </div>  
        <div class="x-titlebar-title" xid="div2">注册</div>  
        <div class="x-titlebar-right reverse" xid="div3"/> 
      </div> 
    </div>  
    <div class="x-panel-content bg" xid="content1"> 
      <div class="wx_loading" id="wxloading"> 
        <div class="wx_loading_inner">请求加载中...</div> 
      </div>  
      <div component="$UI/system/components/justep/controlGroup/controlGroup"
        class="x-control-group" title="title" xid="controlGroup3"> 
        <div component="$UI/system/components/justep/row/row" class="x-row yanzheng"
          xid="row1" style="margin-top:0px;"> 
          <div class="x-col" xid="col2"> 
            <input component="$UI/system/components/justep/input/input" class="form-control"
              xid="tel" placeHolder="输入手机号" style="font-size:16px;padding-left:0px;margin-top:0px;"/> 
          </div>  
          <div class="x-col x-col-fixed" xid="col3" style="width:auto;"> 
            <a component="$UI/system/components/justep/button/button" class="btn yanzhengbtn"
              xid="button4" style="font-size:16px;" label="获取验证码" onClick="button4Click"> 
              <i xid="i4"/>  
              <span xid="span4">获取验证码</span> 
            </a> 
          </div> 
        </div>  
        <div xid="div6" class="borde" style="margin-top:-10px;"/>  
        <input component="$UI/system/components/justep/input/input" class="form-control"
          xid="duanxin" placeHolder="短信验证码" style="font-size:16px;"/>  
        <div xid="div9" class="borde"/>  
        <input component="$UI/system/components/justep/input/input" class="form-control"
          xid="pass" placeHolder="设置密码" style="font-size:16px;"/>  
        <div xid="div6" class="borde"/>  
        <input component="$UI/system/components/justep/input/input" class="form-control"
          xid="rpass" placeHolder="确认密码" style="font-size:16px;"/>  
        <div xid="div6" class="borde"/>  
        <div style="width:100%;height:16px;"/>  
        <div xid="paySection" style="height:40px; padding-top: 5px;color:#666;"> 
          <span component="$UI/system/components/justep/select/radioGroup" class="x-radio-group"
            xid="payTypeSelect" bind-itemset="payData" bind-itemsetValue="ref('fPayType')"
            bind-itemsetLabel="ref('fPayTypeLabel')" style="float:left;margin-left:15px;color:#666;"/> 
        </div>  
        <div style="width:100%;height:16px;"/>  
        <div xid="div9" class="borde"/>  
        <a component="$UI/system/components/justep/button/button" class="btn btn-default btn-block"
          label="注册" xid="regBtn" onClick="regBtnClick"> 
          <i xid="i5"/>  
          <span xid="span5">注册</span> 
        </a>  
        <!-- <div xid="div10" class="name" style="margin-top:10px;">点击注册表示同意
          <a>用户服务协议</a>
        </div>  --> 
      </div> 
    </div>  

  </div> 
</div>
