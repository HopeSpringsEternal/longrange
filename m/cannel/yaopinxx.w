<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="left:18px;top:83px;height:244px;"
    onParamsReceive="modelParamsReceive"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="shuju" idColumn="id"> 
      <column name="id" type="String" xid="xid9"/>  
      <column name="title" type="String" xid="xid10"/>  
      <column name="titlemeng" type="String" xid="xid11"/>  
      <column name="content" type="String" xid="xid12"/>  
      <column name="contentmeng" type="String" xid="xid13"/>  
      <column name="number" type="String" xid="column1"/>  
      <column name="status" type="String" xid="column2"/>  
      <column name="drugurl" type="String" xid="column3"/>  
      <data xid="default1">[]</data></div>
  </div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-top" xid="top1"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" title=""
        class="x-titlebar"> 
        <div class="x-titlebar-left"> 
          <a component="$UI/system/components/justep/button/button" label=""
            class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
            xid="backBtn"> 
            <i class="icon-chevron-left"/>  
            <span/> 
          </a> 
        </div>  
        <div class="x-titlebar-title"/>  
        <div class="x-titlebar-right reverse">
          <a component="$UI/system/components/justep/button/button" class="btn btn-default btn-only-icon"
            label="button" xid="button4" icon="icon-android-more"> 
            <i xid="i1" class="icon-android-more"/>  
            <span xid="span1"/>
          </a>
        </div> 
      </div> 
    </div>  
    <div class="x-panel-content bigbg" xid="content1"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel2"> 
        <div class="x-panel-top" xid="top2" style="width:100%;height:auto"> 
          <div class="media" xid="media1" style="background:#fff;height:145px;"> 
            <div class="media-left" xid="mediaLeft1" bind-click="mediaLeft1Click"> 
              <a href="#" xid="a1"> 
                <img class="media-object" src="" alt="" xid="image1" style="width:150px;height:120px;margin-top:1rem;border-radius:8px;margin-left:6px;"
                  bind-attr-src=' $model.shuju.val("drugurl")'/> 
              </a> 
            </div>  
            <div class="media-body" xid="mediaBody1"> 
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row1"> 
                <div class="x-col x-col-fixed x-col-center gggg" xid="col1"
                  style="width:auto;height:120px;">
                  <h4 xid="h42" class="gggg" bind-text='$model.shuju.val("titlemeng")'/>
                </div>  
                <div class="x-col" xid="col2"> 
                  <div component="$UI/system/components/justep/row/row" class="x-row"
                    xid="row2" style="height:120px;float:left;"> 
                    <div class="gggg" xid="col4" style="height:auto;"> 
                      <span xid="span3" class="gggg">ᠰᠠᠩ  ᠪᠠᠢᠬᠤ   ᠲᠤᠭ᠎ᠠ ᠄</span> 
                      <span xid="span4" bind-text='$model.shuju.val("number")'/>  
                
                    </div> 
                  </div> 
                </div> 
              </div> 
            </div> 
          </div> 
        </div>  
        <div class="x-panel-content" xid="content2" style="background:#fff;margin-top:124px;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row3"> 
            <div class="x-col x-col-10" xid="col5" style="background:#fffddd;height:20rem;"> 
              <div class="x-col x-col-fixed x-col-center" xid="col13"> 
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row7"> 
                  <div class="x-col" xid="col19" style="border-bottom:1px solid #e7ebec;"> 
                    <span xid="span13" class="gggg" style="margin-left:-10px;">ᠡᠮ  ᠦᠨ ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</span> 
                  </div> 
                </div>  
                <div component="$UI/system/components/justep/row/row" class="x-row"
                  xid="row8"> 
                  <div class="x-col" xid="col22" style="max-width:16px;margin-left:-7px;"> 
                    <span xid="span14" style="margin-top:10px;">药品介绍</span> 
                  </div> 
                </div> 
              </div> 
            </div>  
            <div class="x-col x-col-90" xid="col6" style="height:36rem;"> 
              <div class="gggg" style="padding:10px" bind-text=' $model.shuju.val("contentmeng")'>
      
      </div>
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
  </div> 
</div>
