define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function(){
		this.callParent();
	};

	Model.prototype.modelParamsReceive = function(event){
      var data=this.comp("zhuanjia");
       data.clear();
       data.loadData([this.params.rowdata]);
       data.first();
	};

	return Model;
});