<?xml version="1.0" encoding="utf-8"?>
<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;" xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:-14px;left:10px;height:auto;" onParamsReceive="modelParamsReceive"> 
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="quipment" idColumn="id"><column name="id" type="String" xid="xid1"></column>
  <column name="img" type="String" xid="xid2"></column>
  <column name="name" type="String" xid="xid3"></column>
  <column name="content" type="String" xid="xid4"></column></div></div>  
  <div component="$UI/system/components/justep/panel/panel" 
    class="x-panel x-full" xid="panel1"> 
      <div class="x-panel-top" xid="top1"> 
        <div component="$UI/system/components/justep/titleBar/titleBar" title="器材展示"
          class="x-titlebar">
          <div class="x-titlebar-left"> 
            <a component="$UI/system/components/justep/button/button"
              label="" class="btn btn-link btn-only-icon" icon="icon-chevron-left"
              onClick="{operation:'window.close'}" xid="backBtn"> 
              <i class="icon-chevron-left"/>  
              <span></span> 
            </a> 
          </div>  
          <div class="x-titlebar-title">器材展示</div>  
          <div class="x-titlebar-right reverse"> 
          </div>
        </div> 
      </div>  
    <div class="x-panel-content" xid="content1"><div xid="div1" class="mainContent" style="background:#fff">
   <div xid="div2" class="Content gggg">
    <div xid="div4" class="Article_Title gggg" bind-text=' $model.quipment.val("name")'></div>
    
    <div xid="div5" style="font-weight:500;overflow-y: auto;font-size:14px;padding-top: 20px;">
     <img src="" alt="" xid="image3" height="100%" class="Img_style" bind-attr-src=' $model.quipment.val("img")'></img>
     <p bind-text=' $model.quipment.val("content")' xid="p1" style="margin-right:10px;font-size:18px;"></p></div> 
    
    </div> </div></div>
  </div> 
</div>