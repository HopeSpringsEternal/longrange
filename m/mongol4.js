define(function(require) {
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function() {
		this.callParent();
	};
	
	//图片路径转换
	Model.prototype.getImageUrl = function(url){
		return require.toUrl(url);
	};
	
	//获取一级分类信息
	/*
	1、默认显示当前一级菜单对应的二、三级数据
	2、点击其它一级菜单，再加载它的二三级数据
	*/
	Model.prototype.rootClassDataCustomRefresh = function(event){		
		/*
		1、加载一级分类数据
		 */
		var url = require.toUrl("./class/json/rootClassData.json");
		var rootClassData = event.source;
		rootClassData.clear();
		$.ajaxSettings.async = false;
		$.getJSON(url, function(data) {
			rootClassData.loadData(data);
		});      
	};
	
	Model.prototype.shujuCustomRefresh = function(event){
		var shuju = this.comp("shuju");
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/getStudyArticlem',
			async: false,
			cache: false,
			success:function(data){
				for(var i in data){
					shuju.newData({
						"defaultValues" : [ {
							"fID": data[i].id ,
							"fenleiid": 2,
							"img":window.imgshou+data[i].coverPhoto,
							"name": data[i].title,
							"yueduliang": data[i].clicks,
							"content":data[i].content
						}]
					});
				}
				
			},
			error:function(e){
			
			}
		})
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/getStudyVideom',
			async: false,
			cache: false,
			success:function(data){
				for(var i in data){
					shuju.newData({
						"defaultValues" : [ {
							"fID": data[i].id ,
							"fenleiid": 1,
							"img":window.imgshou+data[i].coverPhoto,
							"name": data[i].title,
							"yueduliang": data[i].clicks,
							"content":data[i].content,
							"video":window.imgshou+data[i].videoUrl
						}]
					});
				}
			},
			error:function(e){
			
			}
		})
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/getTaskm',
			async: false,
			cache: false,
			success:function(data){
				for(var i in data){
					shuju.newData({
						"defaultValues" : [ {
							"fID": data[i].id ,
							"fenleiid": 4,
							"img":'images/924da128d58d1979!600x600.jpg',
							"name": data[i].title,
							"yueduliang": "0",
							"content":data[i].content,
							"diagnose":data[i].processingMethod,
						}]
					});
				}
			},
			error:function(e){
			
			}
		})
	};
	
	//商品点击事件
	Model.prototype.li2Click = function(event){
		var row = event.bindingContext.$object.toJson();
		var classify_id = row.fenleiid.value;
		if(classify_id == 1){
			this.comp("video").open({
		        params:{
		              rowdata:row
		        }
	        });  
		}else if(classify_id == 2){
			this.comp("img_text").open({
		        params:{
		              rowdata:row
		        }
	        }); 
		}else if(classify_id == 3){
			this.comp("live_broadcast").open({
		        params:{
		              rowdata:row
		        }
	        }); 
		}else{
			this.comp("cases").open({
		        params:{
		              rowdata:row
		        }
	        }); 
		}
	};
	return Model;
});