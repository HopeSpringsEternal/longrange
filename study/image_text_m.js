define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
	
	var Model = function(){
		this.callParent();
	};

	Model.prototype.modelParamsReceive = function(event){
		var data=this.comp("shuju");
		data.clear();
		data.loadData([this.params.rowdata]);
		data.first();
		var childrenid = data.toJson().rows[0].userdata.id.value;
		var comment = this.comp("data1");
		comment.clear();
		$.ajax({
			type:"POST",
			url:window.yuming+'/app//commentm/getCBycid',
			async: false,
			cache: false,
			dataType: 'json',
			data:{ cateId:1,childrenId: childrenid},
			success:function(datas){
				for(var i in datas){
					comment.newData({
						"defaultValues" : [ {
							"id": datas[i].id ,
							"content": datas[i].content,
							"time":datas[i].createTime
						}]
					});
				}
				
				if((comment.toJson().rows).length == 0){
					$("#commenti").html("暂无评论！！")
				}
			},
			error:function(e){
			}
		})
	};

	Model.prototype.button2Click = function(event){
		if(!window.token){
			var tuiyao = this.comp("login");
	         var auditing = this.comp("dengluzhuantai");
	         var row = auditing.getLastRow();
			 tuiyao.open({
				"data" : {
					"rowData" : row.toJson()
				}
			});
		}else{
			var count = this.comp("shuju");
			var childrenid = count.toJson().rows[0].userdata.id.value;
			var content = $("#content").val();
			$.ajax({
				type:"POST",
				url:window.yuming+'/commentms/App/saveCom?login-token='+window.token,
				async: false,
				cache: false,
				dataType: 'text',
				data:{ cateId: 1,chlidrenId: childrenid ,content:content},
				success:function(datas){
					justep.Util.hint('评论成功！',{type:'info',delay:3000,position:'top'});
				},
				error:function(e){
				}
			})
		}
	};

	Model.prototype.dengluzhuantaiCustomRefresh = function(event){
		var dengz=this.comp("dengluzhuantai");
		 if(window.token != null){
		   window.userstatus=1;
		 }else{
		   window.userstatus=0;
		 }
		var zuotis = {
	        defaultValues : [
		        { "id":justep.UUID.createUUID(),
		          "username":window.name,
		          "userstatus":window.userstatus
		        }
	        ]
        };
        dengz.newData(zuotis);
	};

	Model.prototype.loginReceive = function(event){

	};

	return Model;
});