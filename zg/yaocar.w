<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" class="main13" component="$UI/system/components/justep/window/window"
  design="device:mobile;" xid="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:55px;left:268px;" onLoad="modelLoad"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="goodsData" idColumn="id" limit="20" confirmRefresh="true" confirmDelete="false" autoNew="true"> 
      <column label="id" name="id" type="String" xid="column1"></column>
  <column label="店铺ID" name="fShopID" type="String" xid="xid1"></column>
  <column label="标题" name="fTitle" type="String" xid="column2"></column>
  <column label="图片" name="fImg" type="String" xid="column3"></column>
  <column label="选择" name="fChoose" type="Integer" xid="xid4"></column>
  <column label="数量" name="fNumber" type="Integer" xid="xid5"></column>
  <rule xid="rule1">
   <col name="fColor" xid="ruleCol1">
    <constraint xid="constraint1">
     <expr xid="default1"></expr></constraint> 
    <calculate xid="calculate1">
     <expr xid="default2"></expr></calculate> 
    <readonly xid="readonly1">
     <expr xid="default6"></expr></readonly> </col> 
   <col name="fSize" xid="ruleCol2">
    <calculate xid="calculate2">
     <expr xid="default3"></expr></calculate> </col> 
   <col name="fSum" xid="ruleCol3">
    <calculate xid="calculate3">
     <expr xid="default4">$row.val('fChoose')==1?$row.val('fPrice')*$row.val('fNumber'):'0'</expr></calculate> </col> 
   <col name="fNumber" xid="ruleCol4">
    <calculate xid="calculate4">
     <expr xid="default5"></expr></calculate> </col> </rule>
  <data xid="default7">[]</data>
  <column label="介绍" name="jieshao" type="String" xid="xid2"></column></div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="calculateData" idColumn="allSum"> 
      <column label="合计" name="allSum" type="String" xid="xid7"/>  
      <column label="总数量" name="allNumber" type="String" xid="xid8"/>  
      <column label="是否返回" name="isBack" type="Integer" xid="xid17"/>  
      <data xid="default8">[{"allSum":"0","isBack":0}]</data>
    </div>
  </div>  
 <span component="$UI/system/components/justep/windowReceiver/windowReceiver"
    xid="windowReceiver1" style="left:149px;top:283px;" onReceive="windowReceiver1Receive"/> 
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full x-card x-has-iosstatusbar"> 
    <div class="x-panel-top" height="48"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" class="x-titlebar"> 
        <div class="x-titlebar-left"> 
          <a component="$UI/system/components/justep/button/button" class="btn btn-link btn-only-icon"
            label="button" xid="backBtn" icon="icon-chevron-left" bind-visible="$model.calculateData.val(&quot;isBack&quot;)==1"
            onClick="backBtnClick"> 
            <i xid="i1" class="icon-chevron-left"/>  
            <span xid="span3"/> 
          </a> 
        </div>  
        <div class="x-titlebar-title"> 
          <span xid="span1"><![CDATA[选择药品]]></span> 
        </div>  
        <div class="x-titlebar-right reverse"><a component="$UI/system/components/justep/button/button" class="btn btn-default btn-only-icon" label="button" xid="button4" icon="linear linear-bubble" onClick="settlementClick">
   <i xid="i4" class="linear linear-bubble"></i>
   <span xid="span4"></span></a></div> 
      </div> 
    </div>  
    <div class="x-panel-content"> 
       
      <div component="$UI/system/components/justep/list/list" class="x-list yaohe"
        xid="list1" data="goodsData"> 
        <ul class="x-list-template" xid="listTemplateUl1"> 
          <li xid="li1" style="height:42rem;float:left;border-left:1px #ddd solid;">
              <!-- 药片图片 -->
              <img src="" alt="" xid="image1" bind-attr-src='$model.getImageUrl(val("fImg"))' class="" style="width:8rem;margin-left:1rem;"></img>
             
         <div component="$UI/system/components/justep/row/row" class="x-row" style="width:10rem;height:100%;"> 
          <!-- 选择药品 -->
        <div class="x-col x-col-fixed" xid="col1" style="width:auto;"> 
          <span component="$UI/system/components/justep/button/checkbox" class="x-checkbox x-radio choose"
            xid="checkbox2" bind-ref="ref('fChoose')" checkedValue="1" style="width:30px;height:30px;"/>
            <div class="tb-numberOperation" xid="div4"> 
            <a component="$UI/system/components/justep/button/button" class="btn x-gray btn-sm btn-only-icon pull-left"
              label="button" xid="button1" icon="icon-android-remove" onClick="reductionBtnClick"> 
              <i xid="i3" class="icon-android-remove"/>  
              <span xid="span13"/> 
            </a>  
            <span bind-text="ref('fNumber')" class="pull-left"/>  
            <a component="$UI/system/components/justep/button/button" class="btn x-gray btn-sm btn-only-icon pull-left"
              label="button" xid="button2" icon="icon-android-add" onClick="addBtnClick"> 
              <i xid="i6" class="icon-android-add"/>  
              <span xid="span29"/> 
            </a> 
          </div> 
        </div>  
         <!-- 增减页面 -->
       <div class="x-col  tb-nopadding" xid="col3"> 
         <span bind-text="ref('fTitle')" class="x-flex text-black h5 tb-nomargin gggg"
            xid="span26" style="line-height:4.6rem;font-size:17px;font-weight:500;color:#f00;"/> 
        </div> 
        <!--  药品标题-->
        <div class="x-col x-col-fixed tb-nopadding" xid="col2" style="width:auto;"> 
           <span bind-text="ref('jieshao')" class="x-flex text-black h5 tb-nomargin gggg"
            xid="span26"/>
        </div>  
        <!--  药品介绍-->
       
      </div>
          </li>
        </ul> 
      </div>
    </div>  

  </div> 
</div>

