define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
    var zz=window.menghan;
	var Model = function(){
		this.callParent();
	};
  //选择默认选择页面
	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
      if(window.menghan == 2){
      $('.wxm').css("border-right","0px solid #2fa4e7");
      $('.wxh').css("border-right","2px solid #2fa4e7"); 
      $('.yxm').css("border-right","0px solid #2fa4e7");
      $('.yxh').css("border-right","0px solid #2fa4e7"); 
      $('.wmxc').css("display","none");
      $('.whxc').css("display","block");
      $('.ymxc').css("display","none");
      $('.yhxc').css("display","none");
      }    
	};
	//转换蒙语样子
	Model.prototype.row4Click = function(event){
	   zz=1;
      $('.wxm').css("border-right","2px solid #2fa4e7");
      $('.wxh').css("border-right","0px solid #2fa4e7"); 
      $('.yxm').css("border-right","0px solid #2fa4e7");
      $('.yxh').css("border-right","0px solid #2fa4e7"); 
      $('.wmxc').css("display","block");
      $('.whxc').css("display","none");
      $('.ymxc').css("display","none");
      $('.yhxc').css("display","none");
	};
	//转换蒙语样子
	Model.prototype.row5Click = function(event){
	 zz=2;
	  $('.wxm').css("border-right","0px solid #2fa4e7");
      $('.wxh').css("border-right","2px solid #2fa4e7"); 
      $('.yxm').css("border-right","0px solid #2fa4e7");
      $('.yxh').css("border-right","0px solid #2fa4e7"); 
      $('.wmxc').css("display","none");
      $('.whxc').css("display","block");
      $('.ymxc').css("display","none");
      $('.yhxc').css("display","none");
	};
	//转换已处理蒙语样子
	Model.prototype.row6Click = function(event){
	   zz=1;
       $('.wxm').css("border-right","0px solid #2fa4e7");
      $('.wxh').css("border-right","0px solid #2fa4e7"); 
      $('.yxm').css("border-right","2px solid #2fa4e7");
      $('.yxh').css("border-right","0px solid #2fa4e7"); 
      $('.wmxc').css("display","none");
      $('.whxc').css("display","none");
      $('.ymxc').css("display","block");
      $('.yhxc').css("display","none");
	};
	//转换已处理蒙语样子
	Model.prototype.row7Click = function(event){
	   zz=2;
	   $('.wxm').css("border-right","0px solid #2fa4e7");
      $('.wxh').css("border-right","0px solid #2fa4e7"); 
      $('.yxm').css("border-right","0px solid #2fa4e7");
      $('.yxh').css("border-right","2px solid #2fa4e7"); 
      $('.wmxc').css("display","none");
      $('.whxc').css("display","none");
      $('.ymxc').css("display","none");
      $('.yhxc').css("display","block");
	};
	//查看蒙语诊断数据
	Model.prototype.li1Click = function(event){	  
     var row=event.bindingContext.$object;
      
       this.comp("yichulimdialog").open({
        params:{
              code:"m",
              rowData:row.toJson()
        }     
       });
	};
	//查看汉语诊断数据
	Model.prototype.li2Click = function(event){
       var row=event.bindingContext.$object;   
       this.comp("yichulimdialog").open({
        params:{
              code:"z",
              rowData:row.toJson()
        }     
       });
	};
	//跳转添加页面
	Model.prototype.newClick = function(event){
     // 调用对话框新增
		 this.comp("editdialog").open({
			"data" : {
				"operator" : "new",
				 "code":"z"
			}
		});
	};
	//蒙语跳转修改页面
	Model.prototype.editmClick = function(event){
       // 调用对话框编辑，并传入当前行数据
		var row = event.bindingContext.$object;
		 console.log(row.toJson())
		this.comp("editdialog").open({
			"data" : {
				"operator" : "edit",
				"code":"m",
				"rowData" : row.toJson()
			}
		});
	};
   //汉语跳转修改页面
	Model.prototype.edithClick = function(event){
       // 调用对话框编辑，并传入当前行数据
		var row = event.bindingContext.$object;
		this.comp("edithdialog").open({
			"data" : {
				"operator" : "edit",
				"code":"h",
				"rowData" : row.toJson()
			}
		});
	};
	Model.prototype.modelLoad = function(event){
	  //获取蒙语诊断表数据
	   var zuoti = this.comp('mzhenduan');
       $.ajax({
              type: "GET",
              url: window.yuming+'/taskms/AppList?login-token='+window.token,     
              async: false,
              cache: false,
              success: function(datas){
                 //console.log(datas);
                ///  var obj = JSON.parse(datas);
                  zuoti.clear();
                   $.each(datas,function(j,m){
                   //  console.log(m)
                       var zuotis = {
                       
                        defaultValues : [
                        { "id" :m.id,
                          "mm" :m.herdsmanId,
                          "zj" :m.expertId,
                          "title" :m.title,
                          "content" :m.content,
                          "fen" :m.illnessCategoryId,
                          "zhen" :m.processingMethod,
                          "good" :m.good,
                          "type" :m.type,
                          "en" :m.encolsure,
                          "status" :m.status,
                          "mutel" :m.husername
                         
                     }
                   ]
                 };
       zuoti.newData(zuotis);
                   })
     
            },
        error: function(XMLHttpRequest){  
        }
    });
    //获取汉语诊断表数据
    var zuotis = this.comp('hzhenduan');
       $.ajax({
              type: "GET",
              url: window.yuming+'/tasks/AppList?login-token='+window.token,     
              async: false,
              cache: false,
              success: function(datas){
                 //console.log(datas);
                ///  var obj = JSON.parse(datas);
                  zuotis.clear();
                   $.each(datas,function(j,m){
                     
                       var zuotiss = {
                        defaultValues : [
                        { "id" :m.id,
                          "mm" :m.herdsmanId,
                          "zj" :m.expertId,
                          "title" :m.title,
                          "content" :m.content,
                          "fen" :m.illnessCategoryId,
                          "zhen" :m.processingMethod,
                          "good" :m.good,
                          "type" :m.type,
                          "en" :m.encolsure,
                          "status" :m.status,
                          "mutel" :m.husername
                         
                     }
                   ]
                 };
       zuotis.newData(zuotiss);
                   })
     
            },
        error: function(XMLHttpRequest){  
        }
    });

     var count = this.comp('mzhenduan').count();
    
     var zong=5*count;
       $('.wmxc').css("width",zong+"rem");
       $('.wmxc').css("background","#fff");
	};
	
	return Model;
});