<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" class="main13" component="$UI/system/components/justep/window/window"
  design="device:mobile;" xid="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:281px;left:2px;"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="goodsData" idColumn="id" limit="20" confirmRefresh="true" confirmDelete="false"
      autoNew="true"> 
      <column label="id" name="id" type="String" xid="column1"/>  
      <column label="店铺ID" name="fShopID" type="String" xid="xid1"/>  
      <column label="标题" name="fTitle" type="String" xid="column2"/>  
      <column label="图片" name="fImg" type="String" xid="column3"/>  
      <column label="选择" name="fChoose" type="Integer" xid="xid4"/>  
      <column label="数量" name="fNumber" type="Integer" xid="xid5"/>  
      <rule xid="rule1"> 
        <col name="fColor" xid="ruleCol1"> 
          <constraint xid="constraint1"> 
            <expr xid="default1"/>
          </constraint>  
          <calculate xid="calculate1"> 
            <expr xid="default2"/>
          </calculate>  
          <readonly xid="readonly1"> 
            <expr xid="default6"/>
          </readonly> 
        </col>  
        <col name="fSize" xid="ruleCol2"> 
          <calculate xid="calculate2"> 
            <expr xid="default3"/>
          </calculate> 
        </col>  
        <col name="fSum" xid="ruleCol3"> 
          <calculate xid="calculate3"> 
            <expr xid="default4">$row.val('fChoose')==1?$row.val('fPrice')*$row.val('fNumber'):'0'</expr>
          </calculate> 
        </col>  
        <col name="fNumber" xid="ruleCol4"> 
          <calculate xid="calculate4"> 
            <expr xid="default5"/>
          </calculate> 
        </col> 
      </rule>  
      <data xid="default7">[{"id":"11","fShopID":"1","fTitle":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 1","fImg":"../images/yaodian.png","fChoose":1,"fNumber":0},{"id":"12","fShopID":"1","fTitle":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 2","fImg":"../images/yaodian.png","fChoose":1,"fNumber":1},{"id":"22","fShopID":"3","fTitle":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 3","fImg":"../images/yaodian.png","fChoose":0,"fNumber":1},{"id":"32","fShopID":"2","fTitle":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 4","fImg":"../images/yaodian.png","fChoose":0,"fNumber":1},{"id":"42","fShopID":"2","fTitle":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 5","fImg":"../images/yaodian.png","fChoose":0,"fNumber":0},{"id":"52","fShopID":"2","fTitle":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 6","fImg":"../images/yaodian.png","fChoose":0,"fNumber":0}]</data>
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="calculateData" idColumn="allSum"> 
      <column label="合计" name="allSum" type="String" xid="xid7"/>  
      <column label="总数量" name="allNumber" type="String" xid="xid8"/>  
      <column label="是否返回" name="isBack" type="Integer" xid="xid17"/>  
      <data xid="default8">[{"allSum":"0","isBack":0}]</data>
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="yaodianlei" idColumn="id"> 
      <column name="id" type="String" xid="xid13"/>  
      <column name="name" type="String" xid="xid14"/>  
      <data xid="default9">[{"id":"1","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 1"},{"id":"2","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 2"},{"id":"3","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 3"},{"id":"4","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 4"},{"id":"5","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 5"},{"id":"6","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 6"},{"id":"7","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 7"},{"id":"8","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 8"},{"id":"9","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 9"},{"id":"10","name":" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 10"}]</data>
    </div>
  </div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full x-card x-has-iosstatusbar"> 
     <div class="x-panel-top" height="48"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" class="x-titlebar"> 
        <div class="x-titlebar-left"> 
          <a component="$UI/system/components/justep/button/button" class="btn btn-link btn-only-icon"
            label="button" xid="backBtn" icon="icon-chevron-left" bind-visible="$model.calculateData.val(&quot;isBack&quot;)==1"
            onClick="backBtnClick"> 
            <i xid="i1" class="icon-chevron-left"/>  
            <span xid="span3"/> 
          </a> 
        </div>  
        <div class="x-titlebar-title"> 
          <span xid="span1"><![CDATA[选择药品]]></span> 
        </div>  
        <div class="x-titlebar-right reverse"></div> 
      </div> 
    </div>   
    <div xid="content" class="x-panel-content x-scroll-view x-cards" supportpulldown="true"> 
      <div class="x-scroll" component="$UI/system/components/justep/scrollView/scrollView"
        xid="scrollView" pullUpLabel=" "> 
        <div class="x-content-center x-pull-down container" xid="div8"> 
          <i class="x-pull-down-img glyphicon x-icon-pull-down" xid="i2"/>  
          <span class="x-pull-down-label" xid="span9">下拉刷新...</span> 
        </div>  
        <div class="x-scroll-content" xid="div7"> 
          <div component="$UI/system/components/justep/list/list" class="x-list bg-white"
            data="goodsData" xid="goodsList" disablePullToRefresh="true" disableInfiniteLoad="true"> 
            <ul class="x-list-template x-min-height" xid="listTemplateUl4"
              componentname="$UI/system/components/justep/list/list#listTemplateUl"
              id="undefined_listTemplateUl4"> 
              <li xid="li4" class="x-min-height tb-goodList" componentname="li(html)"
                id="undefined_li4"> 
                <div component="$UI/system/components/justep/row/row" class="x-row"> 
                  <div class="x-col x-col-fixed" xid="col1" style="width:auto;">
                    <span component="$UI/system/components/justep/button/checkbox"
                      class="x-checkbox x-radio choose" xid="checkbox2" bind-ref="ref('fChoose')"
                      checkedValue="1"/>
                  </div>  
                  <div class="x-col x-col-fixed tb-nopadding" xid="col2">
                    <img src="" alt="" xid="image1" bind-attr-src="$model.getImageUrl(val(&quot;fImg&quot;))"
                      class="tb-img-good" bind-click="listClick"/>
                  </div>  
                  <div class="x-col  tb-nopadding" xid="col3">
                    <span bind-text="ref('fTitle')" class="x-flex text-black h5 tb-nomargin"
                      xid="span26"/>  
              
                    
                    <div class="tb-numberOperation" xid="div4"> 
                      <a component="$UI/system/components/justep/button/button"
                        class="btn x-gray btn-sm btn-only-icon pull-left" label="button"
                        xid="button1" icon="icon-android-remove" onClick="reductionBtnClick"> 
                        <i xid="i3" class="icon-android-remove"/>  
                        <span xid="span13"/>
                      </a>  
                      <span bind-text="ref('fNumber')" class="pull-left"/>
                      <a component="$UI/system/components/justep/button/button"
                        class="btn x-gray btn-sm btn-only-icon pull-left" label="button"
                        xid="button2" icon="icon-android-add" onClick="addBtnClick"> 
                        <i xid="i6" class="icon-android-add"/>  
                        <span xid="span29"/>
                      </a> 
                    </div>
                  </div>
                </div>
              </li> 
            </ul> 
          </div> 
        </div>  
        <div class="x-content-center x-pull-up" xid="div7"> 
          <span class="x-pull-up-label" xid="span8"/> 
        </div> 
      </div> 
    </div>  
    <div class="x-panel-bottom" xid="bottom1" style="height:4rem"> 
      <div component="$UI/system/components/justep/row/row" class="x-row tb-nopadding"
        xid="row2"> 
        <div class="x-col x-col-20 x-col-center" xid="col4">
          <span component="$UI/system/components/justep/button/checkbox" class="x-checkbox"
            xid="allChoose" label="全选" checked="false" onChange="allChooseChange"/>
        </div>  
        <div class="x-col" xid="col8">
          <div class="text-right" xid="div9"></div>  
          <div class="text-right" xid="div10"></div> 
        </div>  
        <div class="x-col x-col-33 text-center" xid="col9" bind-click="settlementClick"> 
          <a component="$UI/system/components/justep/button/button" class="btn btn-default btn-only-icon tb-settlement"
            label="button" xid="button3" style="width:100%;border-color:#fd5001;height:4rem;"
            icon="linear linear-bubble"> 
            <i xid="i2" class="linear linear-bubble"/>  
            <span xid="span2"/>
          </a>  
          <!-- <span xid="span10">提交</span> --> 
        </div> 
      </div>  
      <span component="$UI/system/components/justep/windowReceiver/windowReceiver"
        xid="wReceiver" style="left:443px;top:568px;"/>
    </div> 
  </div> 
</div>
