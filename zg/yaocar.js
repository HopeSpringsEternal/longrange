define(function(require) {
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
	var Model = function() {
		this.callParent();
		this.isBack;	
	};
	
	//返回上一页
	Model.prototype.backBtnClick = function(event){
		justep.Shell.closePage();
		setTimeout(function(){
			justep.Shell.fireEvent("onRestoreContent", {});
		},500);
	};
	
	//图片路径转换
	Model.prototype.getImageUrl = function(url){	
		return require.toUrl(url);		
	};

	 Model.prototype.windowReceiver1Receive = function(event) {
		// 对话框接收参数后，新增或编辑
		var zuoti = this.comp("goodsData");
		zuoti.clear();
		this.id = event.data.id;
		$.ajax({
              type: "GET",
              url: window.yuming+'/drugs/App/getByDid?login-token='+window.token+'&Did='+this.id,     
              async: false,
              cache: false,
              success: function(datas){
               
                ///  var obj = JSON.parse(datas);
                  zuoti.clear();
                   $.each(datas,function(j,m){
                       var zuotis = {
                        defaultValues : [
                        { "id":m.id,
                          "fShopID":m.uid,
                          "fImg":window.imgshou+m.drugUrl,
                          "fTitle":m.titlemeng,
                          "fChoose":0,
                          "jieshao":m.contentmeng,
                          "fNumber":0
                        }
                        ]
                        };
                        zuoti.newData(zuotis);
                   })
     
            },
        error: function(XMLHttpRequest){  
        }
    });
	    
	};
	//全选
	Model.prototype.allChooseChange = function(event){
		/*
		1、全选多选框绑定变化事件onChange()
		2、点击全选多选框按钮，获取其值
		3、修改商品表中的fChoose字段为全选多选框按钮的值
		*/
		var goodsData = this.comp("goodsData");
		var choose=this.comp("allChoose").val();
		goodsData.each(function(obj){
			if(choose){				
				goodsData.setValue("fChoose","1",obj.row);
			} else {
				goodsData.setValue("fChoose","",obj.row);
			}	
		});
	};
	
	//减数量
	Model.prototype.reductionBtnClick = function(event){		
		/*
		1、减少数量按钮绑定点击事件onClick()
		2、点击按钮，当前记录的fNumber值减1
		3、fNumber为1时不再相减
		*/
		var row = event.bindingContext.$object;
		var n=row.val("fNumber");
		if(n>1){
			row.val("fNumber",n-1);
		}
	};
	
	//加数量
	Model.prototype.addBtnClick = function(event){
		/*
		1、增加数量按钮绑定点击事件onClick()
		2、点击按钮，当前记录的fNumber值加1
		*/
		var row = event.bindingContext.$object;
		var n=row.val("fNumber");
		row.val("fNumber",n+1);
	};
	

	
	Model.prototype.showBackBtn = function(isBack){
		/*
		1、根据参数修改calculateData
		 */		
		this.isBack = isBack;
		var v = isBack ? 1 : 0;
		this.comp("calculateData").setValue("isBack",v);		
	};
	
	//提交选择药品
	Model.prototype.settlementClick = function(event){
          // this.close();
	     this.comp("windowReceiver1").windowEnsure();

	};
    //修改宽度
	Model.prototype.modelLoad = function(event){
        /* var count = this.comp('goodsData').count();
         var zong=11*count;
         $('.yaohe').css("width",zong+"rem");*/
       
	};

	return Model;
});