define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function(){
	   
		this.callParent();
	};
  
    //新添加诊断表的时候蒙汉转换；
	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
      if(window.menghan == 2){
      $('.xm').css("border-right","0px solid #2fa4e7");
      $('.xh').css("border-right","2px solid #2fa4e7");
      $('.mxc').css("display","none");
      $('.hxc').css("display","block");
      }
       var zuoti = this.comp('bingfen');
           zuoti.clear();
       $.ajax({
              type: "GET",
              url: window.yuming+'/illnesscategoryms/AppList?login-token='+window.token,     
              async: false,
              cache: false,
              success: function(datas){
               
                  zuoti.clear();
                   $.each(datas,function(j,m){
                       var zuotis = {
                        defaultValues : [
                        { "id" :m.id,
                          "name" :m.name
                         
                     }
                   ]
                 };
       zuoti.newData(zuotis);
                   })
     
            },
        error: function(XMLHttpRequest){  
        }
    });
      
	};

	//新添加诊断表的时候蒙汉转换；
	Model.prototype.row4Click = function(event){
      $('.xm').css("border-right","2px solid #2fa4e7");
      $('.xh').css("border-right","0px solid #2fa4e7"); 
      $('.mxc').css("display","block");
      $('.hxc').css("display","none");
	};
	//新添加诊断表的时候蒙汉转换；
	Model.prototype.row5Click = function(event){
	  $('.xm').css("border-right","0px solid #2fa4e7");
      $('.xh').css("border-right","2px solid #2fa4e7");
      $('.mxc').css("display","none");
      $('.hxc').css("display","block");
	};
    //接受数据
	Model.prototype.windowReceiver1Receive = function(event) {
		// 对话框接收参数后，新增或编辑
		var accountData = this.comp("mzhenduan");
		var hqmumin = this.comp("hqmumin");
		accountData.clear();
		this.operator = event.data.operator;
		this.code = event.data.code;
		//新增出现弹出框，先获取牧民信息
		if (this.operator == "new") {   
			hqmumin.open();
		} else if (this.operator == "edit") {
		
			accountData.loadData([ event.data.rowData ]);
			accountData.first();
			if(this.code == "m"){
			      $('.xm').css("display","block");
			      $('.xh').css("display","none");
			      $('.mxc').css("display","block");
			      $('.hxc').css("display","none");
			      $('.zh').css("height","11rem");
			}else{
			      $('.xm').css("display","none");
			      $('.xh').css("display","block");
			      $('.mxc').css("display","none");
			      $('.hxc').css("display","block");
			      $('.zh').css("height","11rem");
			}
		}
	};
	//推荐药片
	Model.prototype.tuiyaobtnClick = function(event){
	var mzhengduan=this.comp("mzhenduan").toJson();
	
	  var tuiyao = this.comp("tuiyaodialog");
	  var id=$('.id').val();
	  
       tuiyao.open({
			"data" : {
				"id" : id,
			}
		});
	};
	
   //蒙语接受药品数据	
	Model.prototype.yaopindialogReceived = function(event){
	  var yuanyaodata=this.comp('yaopian');
	  var yanpind=this.comp('yaopindialog').getInnerModel();
	  var xuanzhong=yanpind.comp('goodsData').toJson().rows;
	
	   $.each(xuanzhong,function(i,m){
	           if(m.fChoose.value == 1 && m.fNumber.value>0){
	              var cuotis = {
                  defaultValues : [
                         {"id" :m.userdata.id.value,
                          "name" :m.fTitle.value,
                          "yaodianid":m.fShopID.value,
                          "num" :m.fNumber.value,
                          "img" :m.fImg.value,
                          "status" :m.fChoose.value
                     }
                   ]
                 };
                 yuanyaodata.newData(cuotis);
	           }
	      }); 
	    //  console.log();
    //this.comp("yaopin").saveData();
	};
	 //获取专家选择药品数据
	  Model.prototype.yaopindialoghReceived = function(event){
	  var yuanyaodata=this.comp('yaopian');
	  var yanpind=this.comp('yaopindialogh').getInnerModel();
	  var xuanzhong=yanpind.comp('goodsData').toJson().rows;

	   $.each(xuanzhong,function(i,m){
	           if(m.fChoose.value == 1 && m.fNumber.value>0){
	              var cuotis = {
                  defaultValues : [
                         {"id" :m.userdata.id.value,
                          "name" :m.fTitle.value,
                          "yaodianid":m.fShopID.value,
                          "num" :m.fNumber.value,
                          "img" :m.fImg.value,
                          "status" :m.fChoose.value
                     }
                   ]
                 };
                 yuanyaodata.newData(cuotis);
	           }
	      }); 
	};
	//汉语推荐药品按钮
	Model.prototype.tuiyaohClick = function(event){  
	var mzhengduan=this.comp("mzhenduan").toJson();
	
      var tuiyao = this.comp("tuidialogh");
	  var id=$('.id').val();
	  alert(id)
       tuiyao.open({
			"data" : {
				"id" : id,
			}
		});
	};
	//发送蒙语诊断数据
	Model.prototype.OKbtnClick = function(event){
        // this.close();
        var yyy=new Array()
        var id=$('.id').val();
        var type=$('.type').val();
        var good=$('.good').val();
        var herdsmanid=$('.mm').val();
        var experdid=$('.zg').val();
        var title=$('.title').val();
        var content=$('.content').val();
        var illnesscategoryid=$('.fen').val();
        var pronessingmethod=$('.fangzi').val();
       
      //  var data=this.comp("mzhenduan").getLastRow().toJson();
         var yaodian=this.comp("yaodianlei").toJson();
         var yanpin=this.comp("yaopian").toJson();
         var k=0;
       //  console.log(yaodian.rows);
        $.each(yaodian.rows,function(j,m){
              $.each(yanpin.rows,function(i,n){
               if(m.userdata.id.value == n.yaodianid.value){
            	   yyy[k] = {
            			   drugstoreId:n.yaodianid.value,
            			   drugId:n.userdata.id.value,
            			   drugName:n.name.value,
            			   drugNumber:n.num.value
            	   }
                 k++;
               }
            })
          })
          
         var data={json:JSON.stringify(yyy),id: id,herdsmanId: herdsmanid,expertId: experdid,title: title,content: content,illnessCategoryId: illnesscategoryid,processingMethod: pronessingmethod,type:type,good:good,enclosure:""};
        
        $.ajax({
                type: "POST",
                url: window.yuming+'/expertinfos/App/updateTaskm?login-token='+window.token,     
		        async: false,
		        cache: false,
		        dataType: 'text',
		        data:data,
		        success: function(datas){
                      this.close();
                 },
                 error: function(XMLHttpRequest, textStatus, errorThrown){
                       alert('添加失败！');
                       /* alert(XMLHttpRequest.status);
                        alert(XMLHttpRequest.readyState);
                        alert(textStatus);*/
                   //  var code=$.parseJSON(XMLHttpRequest.responseText);        
                     
         
                 }
                 });
	};
	
	return Model;
});