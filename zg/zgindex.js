define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function(){
		this.callParent();
	};

	Model.prototype.modelModelConstruct = function(event){
      $('.name').html(window.name);
	};
	
	Model.prototype.li2Click = function(event){
      justep.Shell.showPage("zgzhenguan");
	};
	
	Model.prototype.li5Click = function(event){
      justep.Shell.showPage("wanshan");
	};
	Model.prototype.li3Click = function(event){
      justep.Shell.showPage("zgwenzhang");
	};
	Model.prototype.li4Click = function(event){
      justep.Shell.showPage("zgzhibo");
	};
	return Model;
});