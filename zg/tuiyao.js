define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
	
	var Model = function(){
		this.callParent();
	};

	Model.prototype.result = function(){
		//这里实现返回的逻辑
	};
    Model.prototype.windowReceiver1Receive = function(event) {
		// 对话框接收参数后，新增或编辑
		var zuoti = this.comp("yaodianlei");
		zuoti.clear();
		this.id = event.data.id;
		$.ajax({
              type: "GET",
              url: window.yuming+'/drugstoreinfos//App/getByTaskIdM/'+this.id+'?login-token='+window.token+'',     
              async: false,
              cache: false,
              success: function(datas){
                  zuoti.clear();
                   $.each(datas,function(j,m){
                       var zuotis = {
                        defaultValues : [
                        { "id":m.uid,
                          "name":m.pharmacyNameMeng 
                        }
                        ]
                        };
                        zuoti.newData(zuotis);
                   })
     
            },
        error: function(XMLHttpRequest){  
        }
    });
	    
	};
	Model.prototype.OKBtnClick = function(event){
		this.comp('wReceiver').windowEnsure(this.result());
	};
    //添加时候点击取消按钮
	Model.prototype.cancelBtnClick = function(event){
	  this.close();  
	};
    //实现 药店信息添加 父级页面，并dialog打开药片选择页面
	Model.prototype.li1Click = function(event){
	   var row=event.bindingContext.$object.toJson();
	  // console.log(row);
	   var models=this.getParentModel();
	   var yaodian=models.comp('yaodianlei');
	   
	  var cuotis = {
                  defaultValues : [
                         {"id" :row.userdata.id.value,
                          "name" :row.name.value,
                        }
                   ]
                 };
       yaodian.newData(cuotis);
	   models.comp('yaopindialog').open({
			"data" : {
				"id" :row.userdata.id.value
			}
		});
       this.close();
	};
	
	return Model;
});