define(function(require){
require('$model/UI2/system/components/justep/model/model');
require('$model/UI2/system/components/justep/loadingBar/loadingBar');
require('$model/UI2/system/components/justep/button/button');
require('$model/UI2/system/components/justep/scrollView/scrollView');
require('$model/UI2/system/components/justep/list/list');
require('$model/UI2/system/components/justep/panel/child');
require('$model/UI2/system/components/justep/panel/panel');
require('$model/UI2/system/components/justep/button/checkbox');
require('$model/UI2/system/components/justep/windowReceiver/windowReceiver');
require('$model/UI2/system/components/justep/row/row');
require('$model/UI2/system/components/justep/titleBar/titleBar');
require('$model/UI2/system/components/justep/data/data');
require('$model/UI2/system/components/justep/window/window');
var __parent1=require('$model/UI2/system/lib/base/modelBase'); 
var __parent0=require('$model/UI2/longrange/zg/yaocarh'); 
var __result = __parent1._extend(__parent0).extend({
	constructor:function(contextUrl){
	this.__sysParam='true';
	this.__contextUrl=contextUrl;
	this.__id='';
	this.__cid='cV326Zr';
	this._flag_='76fd608b789b5d4aca7f92a55d880da5';
	this.callParent(contextUrl);
 var __Data__ = require("$UI/system/components/justep/data/data");new __Data__(this,{"autoLoad":true,"confirmDelete":false,"confirmRefresh":true,"defCols":{"fChoose":{"define":"fChoose","label":"选择","name":"fChoose","relation":"fChoose","rules":{"integer":true},"type":"Integer"},"fImg":{"define":"fImg","label":"图片","name":"fImg","relation":"fImg","type":"String"},"fNumber":{"define":"fNumber","label":"数量","name":"fNumber","relation":"fNumber","rules":{"integer":true},"type":"Integer"},"fShopID":{"define":"fShopID","label":"店铺ID","name":"fShopID","relation":"fShopID","type":"String"},"fTitle":{"define":"fTitle","label":"标题","name":"fTitle","relation":"fTitle","type":"String"},"id":{"define":"id","label":"id","name":"id","relation":"id","type":"String"}},"directDelete":false,"events":{},"idColumn":"id","initData":"[{\"id\":\"11\",\"fShopID\":\"1\",\"fTitle\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 1\",\"fImg\":\"../images/yaodian.png\",\"fChoose\":1,\"fNumber\":0},{\"id\":\"12\",\"fShopID\":\"1\",\"fTitle\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 2\",\"fImg\":\"../images/yaodian.png\",\"fChoose\":1,\"fNumber\":1},{\"id\":\"22\",\"fShopID\":\"3\",\"fTitle\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 3\",\"fImg\":\"../images/yaodian.png\",\"fChoose\":0,\"fNumber\":1},{\"id\":\"32\",\"fShopID\":\"2\",\"fTitle\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 4\",\"fImg\":\"../images/yaodian.png\",\"fChoose\":0,\"fNumber\":1},{\"id\":\"42\",\"fShopID\":\"2\",\"fTitle\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 5\",\"fImg\":\"../images/yaodian.png\",\"fChoose\":0,\"fNumber\":0},{\"id\":\"52\",\"fShopID\":\"2\",\"fTitle\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 6\",\"fImg\":\"../images/yaodian.png\",\"fChoose\":0,\"fNumber\":0}]","limit":20,"xid":"goodsData"});
 new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"allNumber":{"define":"allNumber","label":"总数量","name":"allNumber","relation":"allNumber","type":"String"},"allSum":{"define":"allSum","label":"合计","name":"allSum","relation":"allSum","type":"String"},"isBack":{"define":"isBack","label":"是否返回","name":"isBack","relation":"isBack","rules":{"integer":true},"type":"Integer"}},"directDelete":false,"events":{},"idColumn":"allSum","initData":"[{\"allSum\":\"0\",\"isBack\":0}]","limit":20,"xid":"calculateData"});
 new __Data__(this,{"autoLoad":true,"confirmDelete":true,"confirmRefresh":true,"defCols":{"id":{"define":"id","name":"id","relation":"id","type":"String"},"name":{"define":"name","name":"name","relation":"name","type":"String"}},"directDelete":false,"events":{},"idColumn":"id","initData":"[{\"id\":\"1\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 1\"},{\"id\":\"2\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 2\"},{\"id\":\"3\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 3\"},{\"id\":\"4\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 4\"},{\"id\":\"5\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 5\"},{\"id\":\"6\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 6\"},{\"id\":\"7\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 7\"},{\"id\":\"8\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 8\"},{\"id\":\"9\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 9\"},{\"id\":\"10\",\"name\":\" ᠴᠡᠯᠡᠭᠡᠷ ᠡᠮ  ᠦᠨ ᠲᠡᠯᠭᠡᠭᠦᠷ 10\"}]","limit":20,"xid":"yaodianlei"});
}}); 
return __result;});