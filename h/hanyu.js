define(function(require){
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");
	/*require("../misc/js/touchpoint");*/
  
	
	var Model = function(){
	  
		this.callParent();
	};
    // 图片路径转换
	Model.prototype.transUrl = function(row) {
		var url = (typeof row === "object") ? "./img/" + row.val("imageName") : row
	    return require.toUrl(url);
	};
	//点击切换语言
	Model.prototype.hanClick = function(event){
      justep.Shell.showPage("main",{yucode : 1});   
	};

	/*Model.prototype.panel1Click = function(event){
       TouchPoint.color = 'red';
	   TouchPoint.init();
	};*/

	//跳转诊断页面
	Model.prototype.col1Click = function(event){
       justep.Shell.showPage("zhenduanlie"); 
	};

	Model.prototype.col2Click = function(event){
        justep.Shell.showPage("xuexilie");
	};

	Model.prototype.col3Click = function(event){
        justep.Shell.showPage("yaodianlie");
	};

	Model.prototype.modelLoad = function(event){
		var bunner = this.comp("bunner");
		var study = this.comp("study");
		var quipment = this.comp("quipment");
		var expert = this.comp("expert");
		expert.clear();
		bunner.clear();
		study.clear();
		quipment.clear();
		$.ajax({
			type:"get",
			url:window.yuming+'/app/getSlidemList',
			asnyc:false,
			cache:false,
			success:function(data){
				bunner.newData({
					"defaultValues":[{
						"img1":window.imgshou+JSON.parse(data[0].pic),
						"img2":window.imgshou+JSON.parse(data[1].pic),
						"img3":window.imgshou+JSON.parse(data[2].pic),
//						"img4":window.imgshou+JSON.parse(data[3].pic)
					}]
				})
			},
			error:function(e){
			}
		})
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/expertinfo/three',
			asnyc:false,
			cache:false,
			success:function(data){
				var id = 0;
				for(var i in data){
					expert.newData({
						"defaultValues":[{
							"id":id,
							"name":data[i].name,
							"img":window.imgshou+data[i].headerUrl,
							"jieshao":data[i].expertise,
							"zhenduan":data[i].dealingProblems,
							"xiang":data[i].personal
						}]
					})
					id++;
				}
			},
			error:function(e){
			}
		})
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/studyvideo/frist',
			asnyc:false,
			cache:false,
			success:function(data){
				var id = 0;
				for(var i in data){
					study.newData({
						"defaultValues":[{
							"id":id,
							"name":data[i].title,
							"img":window.imgshou+data[i].coverPhoto,
							"yueduliang": data[i].clicks,
							"content":data[i].content,
							"video":window.imgshou+data[i].videoUrl
						}]
					})
					id++;
				}
			},
			error:function(e){
			}
		})
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/medicalequipment/all',
			asnyc:false,
			cache:false,
			success:function(data){
				var id = 0;
				for(var i in data){
					quipment.newData({
						"defaultValues":[{
							"id":id,
							"img":window.imgshou+data[i].image,
							"name":data[i].name,
							"content":data[i].content
						}]
					})
					id++;
				}
			},
			error:function(e){
			}
		})
	};

	Model.prototype.col33Click = function(event){
		var rows=event.bindingContext.$object;
		this.comp("expertDialog").open({
	        params:{
	              rowdata:rows.toJson()
	        }     
		});
	};

	Model.prototype.col30Click = function(event){
		var rows=event.bindingContext.$object;
		this.comp("studyDialog").open({
	        params:{
	              rowdata:rows.toJson()
	        }     
		});
	};
	Model.prototype.col32Click = function(event){
		 var rows=event.bindingContext.$object;
		this.comp("quipmentDialog").open({
	        params:{
	              rowdata:rows.toJson()
	        }     
		});
	};
	return Model;
});