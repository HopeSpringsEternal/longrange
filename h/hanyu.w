<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="top:2px;left:162px;height:auto;" onLoad="modelLoad"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="yiqi" idColumn="ID">
      <column name="ID" type="String" xid="xid4"/>  
      <column name="name" type="String" xid="xid5"/>  
      <column name="img" type="String" xid="xid6"/>  
      <data xid="default1">[{"ID":"0","name":"通拉嘎","img":"./images/xue1.jpg"},{"ID":"1","name":"通拉嘎","img":"./images/xue1.jpg"},{"ID":"2","name":"通拉嘎","img":"./images/xue1.jpg"}]</data> 
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="zhuanjia" idColumn="ID"> 
      <column name="ID" type="String" xid="xid1"/>  
      <column name="name" type="String" xid="xid2"/>  
      <column name="img" type="String" xid="xid3"/>  
      <data xid="default1">[{"ID":"0","name":"通拉嘎","img":"./images/11.png"},{"ID":"1","name":"通拉嘎","img":"./images/12.png"},{"ID":"2","name":"通拉嘎","img":"./images/13.png"}]</data>
    </div>  
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="qicai" idColumn="ID">
      <column name="ID" type="String" xid="xid7"/>  
      <column name="name" type="String" xid="xid8"/>  
      <column name="img" type="String" xid="xid9"/>  
      <data xid="default1">[{"ID":"0","name":"通拉嘎","img":"./images/xue1.jpg"},{"ID":"1","name":"通拉嘎","img":"./images/xue1.jpg"},{"ID":"2","name":" 通拉嘎ᠷ","img":"./images/xue1.jpg"}]</data> 
    </div>
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="bunner" idColumn="img1"><column name="img1" type="String" xid="xid10"></column>
  <column name="img2" type="String" xid="xid11"></column>
  <column name="img3" type="String" xid="xid12"></column>
  <column name="img4" type="String" xid="xid13"></column></div>
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="study" idColumn="id"><column name="id" type="String" xid="xid14"></column>
  <column name="name" type="String" xid="xid15"></column>
  <column name="img" type="String" xid="xid16"></column>
  <column name="yueduliang" type="String" xid="xid26"></column>
  <column name="content" type="String" xid="xid27"></column>
  <column name="video" type="String" xid="xid28"></column></div>
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="quipment" idColumn="id"><column name="id" type="String" xid="xid17"></column>
  <column name="img" type="String" xid="xid18"></column>
  <column name="name" type="String" xid="xid19"></column>
  <column name="content" type="String" xid="xid29"></column></div>
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="expert" idColumn="id"><column name="id" type="String" xid="xid20"></column>
  <column name="name" type="String" xid="xid21"></column>
  <column name="img" type="String" xid="xid22"></column>
  <column name="jieshao" type="String" xid="xid23"></column>
  <column name="zhenduan" type="String" xid="xid24"></column>
  <column name="xiang" type="String" xid="xid25"></column></div></div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-top" xid="top1"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" title="远程治疗"
        class="x-titlebar"> 
        <div class="x-titlebar-left"> 
          <!-- <a component="$UI/system/components/justep/button/button"
              label="" class="btn btn-link btn-only-icon" icon="icon-chevron-left"
              onClick="{operation:'window.close'}" xid="backBtn"> 
              <i class="icon-chevron-left"/>  
              <span></span> 
            </a>  --> 
        </div>  
        <div class="x-titlebar-title">远程治疗</div>  
        <div class="x-titlebar-right reverse"> 
          <a component="$UI/system/components/justep/button/button" class="btn btn-default"
            label="切换蒙语版" xid="hanbtn" onClick="hanClick"> 
            <i xid="i1"/>  
            <span xid="span1"/>
          </a>
        </div> 
      </div> 
    </div>  
    <div class="x-panel-content bigbg" xid="content1"> 
      <!-- 幻灯图片开始-->  
      <div component="$UI/system/components/bootstrap/carousel/carousel" class="x-carousel carousel bannergao"
        xid="carousel1"> 
        <ol class="carousel-indicators" xid="ol1"/>  
        <div class="x-contents carousel-inner" role="listbox" component="$UI/system/components/justep/contents/contents"
          active="0" slidable="true" wrap="true" swipe="true" xid="imgcontents" bind-click="contentstoClick"> 
          <div class="x-contents-content" xid="content7"> 
            <img class="img-responsive x-img1 bannergaoimg" xid="image1" src="$UI/longrange/img/1.jpg" bind-attr-src=' $model.bunner.val("img1")'/> 
          </div>  
          <div class="x-contents-content" xid="content8"> 
            <img class="img-responsive x-img1 bannergaoimg" src="$UI/longrange/img/2.jpg"
              xid="image2" bind-attr-src=' $model.bunner.val("img2")'/> 
          </div>  
          <div class="x-contents-content" xid="content9"> 
            <img class="img-responsive x-img1 bannergaoimg" src="$UI/longrange/img/3.jpg"
              xid="image3" bind-attr-src=' $model.bunner.val("img3")'/> 
          </div>  
          <div class="x-contents-content" xid="content10" url="./contents/content4.w"> 
            <img class="img-responsive x-img1 bannergaoimg" src="$UI/longrange/img/2.jpg"
              xid="image4"/> 
          </div> 
        </div> 
      </div>  
      <!--  幻灯图片结束-->  
      <!--  专家-->  
      <div component="$UI/system/components/justep/row/row" class="x-row xia"
        xid="row1" style="max-width:100%"> 
        <div class="x-col x-col-33" xid="col1" bind-click="col1Click"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row2"> 
            <div class="x-col x-col-fixed x-col-center" xid="col4" style="width:auto;"> 
              <img src="$UI/longrange/img/shipin.png" alt="" xid="image5" class="suoxiao"/>
            </div>  
            <div class="x-col x-col-fixed x-col-center" xid="col5" style="width:auto;padding-right:0px;padding-left:0px;"> 
              <span xid="span7" class="gggg">ᠤᠨᠤᠴᠢᠯᠠᠬᠤ</span>
            </div>  
            <div class="x-col x-col-fixed x-col-center" xid="col6" style="width:auto;max-width:16px"> 
              <span xid="span8">诊断</span>
            </div> 
          </div> 
        </div>  
        <div class="x-col x-col-33" xid="col2" bind-click="col2Click"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row3"> 
            <div class="x-col x-col-fixed x-col-center" xid="col7" style="width:auto;"> 
              <img src="$UI/longrange/img/xuexi.png" alt="" xid="image6" class="suoxiao"/>
            </div>  
            <div class="x-col x-col-fixed x-col-center" xid="col8" style="width:auto;padding-right:0px;padding-left:0px;"> 
              <span xid="span9" class="gggg">ᠰᠤᠷᠤᠯᠴᠠᠬᠤ</span>
            </div>  
            <div class="x-col x-col-fixed x-col-center" xid="col9" style="width:auto;max-width:16px"> 
              <span xid="span10">学习</span>
            </div> 
          </div> 
        </div>  
        <div class="x-col x-col-33" xid="col3" bind-click="col3Click"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row4"> 
            <div class="x-col x-col-fixed x-col-center" xid="col10" style="width:auto;"> 
              <img src="$UI/longrange/img/yao.png" alt="" xid="image7" class="suoxiao"/>
            </div>  
            <div class="x-col x-col-fixed x-col-center" xid="col11" style="width:auto;padding-right:0px;padding-left:0px;"> 
              <span xid="span11" class="gggg">ᠳᠡᠯᠭᠡᠭᠦᠷ</span>
            </div>  
            <div class="x-col x-col-fixed x-col-center" xid="col12" style="width:auto;max-width:16px"> 
              <span xid="span12">药店</span>
            </div> 
          </div> 
        </div> 
      </div>
      <div component="$UI/system/components/justep/row/row" class="x-row zhuanjia"
        xid="row5"> 
        <div class="x-col x-col-fixed x-col-center" xid="col13" style="width:auto;max-width:24px;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row7"> 
            <div class="x-col" xid="col19" style="border-bottom:1px solid #e7ebec;margin-top:-4em"> 
              <span xid="span13" class="gggg" style="margin-left:-10px;;margin-top:56px;">ᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ</span> 
            </div> 
          </div>  
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row8"> 
            <div class="x-col" xid="col22" style="max-width: 16px;margin-left: -7px;"> 
              <span xid="span14" style="margin-top:10px;">专家</span> 
            </div> 
          </div> 
        </div>  
        <div class="x-col" xid="col15"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row16"> 
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="expert" filter=' $row.val("id") == 0' style="width:33.33%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col33" bind-click="col33Click">
                  <img alt="" xid="image8" class="img radius" bind-attr-src="val(&quot;img&quot;)" style="height:145px;"/>
                  <div class="mingzi" bind-text="val(&quot;name&quot;)"></div>
                </div> 
              </ul> 
            </div>  
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="expert" filter=' $row.val("id") == 1' style="width:33.33%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col34" bind-click="col33Click">
                  <img alt="" xid="image8" class="img radius" bind-attr-src="val(&quot;img&quot;)" style="height:145px;"/>
                  <div class="mingzi" bind-text="val(&quot;name&quot;)"></div>
                </div> 
              </ul> 
            </div>  
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="expert" filter=' $row.val("id") == 2' style="width:33.33%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col35" bind-click="col33Click">
                  <img alt="" xid="image8" class="img radius" bind-attr-src="val(&quot;img&quot;)" style="height:145px;"/>
                  <div class="mingzi" bind-text="val(&quot;name&quot;)"></div>
                </div> 
              </ul> 
            </div> 
          </div> 
        </div> 
      </div>  
      <!-- 学习视频 -->  
      <div component="$UI/system/components/justep/row/row" class="x-row xuexi"
        xid="row5"> 
        <div class="x-col x-col-fixed x-col-center" xid="col13" style="width:auto;max-width:24px;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row7"> 
            <div class="x-col" xid="col19" style="border-bottom:1px solid #e7ebec;margin-top:-4em"> 
              <span xid="span13" class="gggg" style="margin-left:-10px;margin-top:56px">ᠰᠤᠷᠤᠯᠴᠠᠬᠤ</span> 
            </div> 
          </div>  
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row8"> 
            <div class="x-col" xid="col22" style="max-width: 16px;margin-left: -7px;"> 
              <span xid="span14" style="margin-top:10px;">学习</span> 
            </div> 
          </div> 
        </div>  
        <div class="x-col x-col-center" xid="col15" style="height:100%;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row x-row-center"
            xid="row16"> 
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="study" filter=' $row.val("id") == 0' style="width:50%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col33" bind-click="col30Click" style="text-align:center;">
                  <img alt="" xid="image8" class="img radius" bind-attr-src="val(&quot;img&quot;)" style="height:145px;"/>
                  <!-- <div class="gggg mingzi" bind-text='val("name")'>ᠲᠤᠩᠭᠠᠯᠠᠭ</div> -->
                <img src="$UI/longrange/images/player.png" alt="" xid="image9" style="width:35px;height:35px;" class="player"></img></div> 
              </ul> 
            </div>  
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="study" filter=' $row.val("id") == 1' style="width:50%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col34" bind-click="col30Click" style="text-align:center;">
                  <img alt="" xid="image8" class="img radius" bind-attr-src="val(&quot;img&quot;)" style="height:145px;"/>
                  <!-- <div class="gggg mingzi" bind-text='val("name")'>ᠲᠤᠩᠭᠠᠯᠠᠭ</div> -->
                <img src="$UI/longrange/images/player.png" alt="" xid="image10" style="width:35px;height:35px;" class="player"></img></div> 
              </ul> 
            </div> 
          </div> 
        </div> 
      </div>  
      <!-- 治疗器材-->  
      <div component="$UI/system/components/justep/row/row" class="x-row qicai"
        xid="row5"> 
        <div class="x-col x-col-fixed x-col-center" xid="col13" style="width:auto;max-width:24px;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row7"> 
            <div class="x-col" xid="col19" style="border-bottom:1px solid #e7ebec;margin-top:-4em"> 
              <span xid="span13" class="gggg" style="margin-left:-10px;margin-top:56px;">ᠮᠠᠰᠢᠨ</span> 
            </div> 
          </div>  
          <div component="$UI/system/components/justep/row/row" class="x-row"
            xid="row8"> 
            <div class="x-col" xid="col22" style="max-width: 16px;margin-left: -7px;"> 
              <span xid="span14" style="margin-top:10px;">器材</span> 
            </div> 
          </div> 
        </div>  
        <div class="x-col" xid="col15"> 
          <div component="$UI/system/components/justep/row/row" class="x-row x-row-center"
            xid="row16" style="height:100%;"> 
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="quipment" filter=' $row.val("id") == 0' style="width:33.33%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col33" bind-click="col32Click">
                  <img alt="" xid="image8" class="img radius" style='max-height:145px;' bind-attr-src="val(&quot;img&quot;)"/>
                </div> 
              </ul> 
            </div>  
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="quipment" filter=' $row.val("id") == 1' style="width:33.33%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col34" bind-click="col32Click">
                  <img alt="" xid="image8" class="img radius" style='max-height:145px;' bind-attr-src="val(&quot;img&quot;)"/>
                </div> 
              </ul> 
            </div>  
            <div component="$UI/system/components/justep/list/list" class="x-list"
              xid="list1" data="quipment" filter=' $row.val("id") == 2' style="width:33.33%;"> 
              <ul class="x-list-template" xid="listTemplateUl1" style="padding:0px;margin:0px"> 
                <div class="x-col" xid="col35" bind-click="col32Click">
                  <img alt="" xid="image8" class="img radius" style='max-height:145px;' bind-attr-src="val(&quot;img&quot;)"/>
                </div> 
              </ul> 
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
  </div> 
<span component="$UI/system/components/justep/windowDialog/windowDialog" xid="expertDialog" src="$UI/longrange/h/cannel/zhuanjia.w"></span>
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="studyDialog" src="$UI/longrange/study/video.w"></span>
  <span component="$UI/system/components/justep/windowDialog/windowDialog" xid="quipmentDialog" src="$UI/longrange/h/cannel/quipment.w"></span></div>
