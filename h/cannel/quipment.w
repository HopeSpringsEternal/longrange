<?xml version="1.0" encoding="utf-8"?>
<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;" xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="left:18px;top:83px;height:244px;" onParamsReceive="modelParamsReceive"> 
  <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="quipment" idColumn="id">
   <column name="id" type="String" xid="xid1"></column>
   <column name="img" type="String" xid="xid2"></column>
   <column name="name" type="String" xid="xid3"></column>
   <column name="content" type="String" xid="xid4"></column></div></div>  
  <div component="$UI/system/components/justep/panel/panel" 
    class="x-panel x-full" xid="panel1"> 
      <div class="x-panel-top" xid="top1"> 
        <div component="$UI/system/components/justep/titleBar/titleBar" title="标题"
          class="x-titlebar">
          <div class="x-titlebar-left"> 
            <a component="$UI/system/components/justep/button/button"
              label="" class="btn btn-link btn-only-icon" icon="icon-chevron-left"
              onClick="{operation:'window.close'}" xid="backBtn"> 
              <i class="icon-chevron-left"/>  
              <span></span> 
            </a> 
          </div>  
          <div class="x-titlebar-title">标题</div>  
          <div class="x-titlebar-right reverse"> 
          </div>
        </div> 
      </div>  
    <div class="x-panel-content" xid="content1"><div xid="div1" class="mainContent" style="background:#fff">
   <div xid="div2" class="Content">
    <div xid="div4" class="Article_Title" bind-text=' $model.quipment.val("name")'></div>
    
    <div xid="div3" style="font-weight:500;overflow-y: auto;font-size:14px;padding-top: 20px;">
     <img src="" alt="" xid="image1" class="center-block" style="width:80%;padding-top:20px;padding-bottom:20px;" bind-attr-src=' $model.quipment.val("img")'></img>
     <p bind-text=' $model.quipment.val("content")' xid="p1"></p></div> 
    
    </div> </div></div>
  </div> 
</div>