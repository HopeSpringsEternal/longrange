<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="height:auto;top:74px;left:392px;"
    onParamsReceive="modelParamsReceive"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true"
      xid="shuju" idColumn="id"> 
      <column name="id" type="String" xid="xid9"/>  
      <column name="title" type="String" xid="xid10"/>  
      <column name="titlemeng" type="String" xid="xid11"/>  
      <column name="content" type="String" xid="xid12"/>  
      <column name="contentmeng" type="String" xid="xid13"/>  
      <column name="number" type="String" xid="column1"/>  
      <column name="status" type="String" xid="column2"/>  
      <column name="drugurl" type="String" xid="column3"/>  
      <data xid="default1">[]</data></div>
  </div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-top" xid="top1"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" title=""
        class="x-titlebar"> 
        <div class="x-titlebar-left"> 
          <a component="$UI/system/components/justep/button/button" label=""
            class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
            xid="backBtn"> 
            <i class="icon-chevron-left"/>  
            <span/> 
          </a> 
        </div>  
        <div class="x-titlebar-title"/>  
        <div class="x-titlebar-right reverse">
          <a component="$UI/system/components/justep/button/button" class="btn btn-default btn-only-icon"
            label="button" xid="button4" icon="icon-android-more"> 
            <i xid="i1" class="icon-android-more"/>  
            <span xid="span1"/>
          </a>
        </div> 
      </div> 
    </div>  
    <div class="x-panel-content bigbg" xid="content1"> 
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel2"> 
        <div class="x-panel-top" xid="top2" style="width:100%;height:145px;"> 
          <div class="media" xid="media1" style="background:#fff;height:145px;"> 
            <div class="media-left" xid="mediaLeft1" bind-click="mediaLeft1Click"> 
              <a href="#" xid="a1"> 
                <img class="media-object" src="" alt="" xid="image1" style="width:150px;height:120px;margin-top:1rem;border-radius:8px;margin-left:6px;"
                  bind-attr-src=' $model.shuju.val("drugurl")'/> 
              </a> 
            </div>  
            <div class="media-body" xid="mediaBody1"> 
              <div component="$UI/system/components/justep/row/row" class="x-row"
                xid="row1"> 
                <div class="x-col" xid="col2" style="margin-top:20px;"> 
                  <h4 xid="h41" bind-text='$model.shuju.val("title")'><![CDATA[]]></h4></div> 
              </div> 
            <div component="$UI/system/components/justep/row/row" class="x-row" xid="row2">
   <div class="x-col" xid="col1"><span xid="span2"><![CDATA[库存量]]></span></div>
   <div class="x-col" xid="col4" bind-text=' $model.shuju.val("number")'></div></div></div> 
          </div> 
        </div>  
        <div class="x-panel-content" xid="content2" style="background:#fff;margin-top:124px;"> 
          <div component="$UI/system/components/justep/row/row" class="x-row x-row-center" xid="row3" style="text-align:center;background:#fffddd;height:40px;padding:0px 0px 0px 0px;line-height:40px;border-bottom: 1px solid #ffe5b6;">
   <div class="x-col x-col-center" xid="col14">
    <span xid="span8"><![CDATA[药品介绍]]></span></div> </div>
  <div component="$UI/system/components/justep/row/row" class="x-row" xid="row9">
   <div class="x-col" xid="col17">
    <div class="" style="padding:10px" bind-text=' $model.shuju.val("content")' xid="div1"></div></div> </div></div> 
      </div> 
    </div> 
  </div> 
</div>
