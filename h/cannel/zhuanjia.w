<?xml version="1.0" encoding="utf-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" component="$UI/system/components/justep/window/window" design="device:m;"
  xid="window" class="window">  
  <div component="$UI/system/components/justep/model/model" xid="model" style="left:18px;top:83px;height:244px;" onParamsReceive="modelParamsReceive"> 
    <div component="$UI/system/components/justep/data/data" autoLoad="true" xid="zhuanjia" idColumn="id">
   <column name="id" type="String" xid="xid1"></column>
   <column name="img" type="String" xid="xid2"></column>
   <column name="name" type="String" xid="xid3"></column>
   <column name="leixing" type="String" xid="xid4"></column>
   <column name="jieshao" type="String" xid="xid5"></column>
   <column name="zhenduan" type="String" xid="xid6"></column>
   <column name="zhuangtai" type="String" xid="xid7"></column>
   <column name="uid" type="String" xid="xid8"></column>
   <column name="xiang" type="String" xid="xid9"></column></div></div>  
  <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
    xid="panel1"> 
    <div class="x-panel-top" xid="top1"> 
      <div component="$UI/system/components/justep/titleBar/titleBar" title=""
        class="x-titlebar"> 
        <div class="x-titlebar-left"> 
          <a component="$UI/system/components/justep/button/button" label=""
            class="btn btn-link btn-only-icon" icon="icon-chevron-left" onClick="{operation:'window.close'}"
            xid="backBtn"> 
            <i class="icon-chevron-left"/>  
            <span/> 
          </a> 
        </div>  
        <div class="x-titlebar-title"/>  
        <div class="x-titlebar-right reverse"><a component="$UI/system/components/justep/button/button" class="btn btn-default btn-only-icon" label="button" xid="button4" icon="icon-android-more">
   <i xid="i1" class="icon-android-more"></i>
   <span xid="span1"></span></a></div> 
      </div> 
    </div>  
    <div class="x-panel-content bigbg" xid="content1">
      <div component="$UI/system/components/justep/panel/panel" class="x-panel x-full"
        xid="panel2"> 
        <div class="x-panel-top" xid="top2" style="width:100%;height:145px;background-color:#FFFFFF;"> 
          <div class="media" xid="media1">
   <div class="media-left" xid="mediaLeft1" bind-click="mediaLeft1Click">
    <a href="#" xid="a1">
     <img class="media-object" src="" alt="" xid="image1" bind-attr-src=' $model.zhuanjia.val("img")' style="width:150px;height:120px;margin-top:1rem;border-radius:8px;margin-left:6px"></img></a> </div> 
   <div class="media-body" xid="mediaBody1">
    <div component="$UI/system/components/justep/row/row" class="x-row" xid="row1" style="padding:10px 0px 0px 0px;">
     <div class="x-col x-col-fixed x-col-center" xid="col1" style="width:auto;padding:0px 0px 0px 0px;">
      <h4 class="media-heading" xid="h41" bind-text='$model.zhuanjia.val("name")'></h4>
      <p xid="p2">
       <span bind-text='$model.zhuanjia.val("jieshao")' xid="span3"></span></p> </div> 
     <div class="x-col" xid="col3">
      <div component="$UI/system/components/justep/row/row" class="x-row" xid="row4">
       <div class="x-col" xid="col12">
        <div component="$UI/system/components/justep/row/row" class="x-row" xid="row6" style="border-bottom:1px #666 solid">
         <div class="x-col" xid="col18">诊断数:</div>
         <div class="x-col" xid="col20" bind-text=' $model.zhuanjia.val("zhenduan")'></div></div> 
        <div component="$UI/system/components/justep/row/row" class="x-row" xid="row5" style="margin-top:1rem;">
         <div class="x-col" xid="col8" style="padding:0px;">
          <a component="$UI/system/components/justep/button/button" class="btn btn-link btn-xs btn-only-icon" label="button" xid="button1" icon="icon-ios7-videocam" style="font-size:24px;" onClick="button1Click">
           <i xid="i2" class="icon-ios7-videocam"></i>
           <span xid="span4"></span></a> </div> 
         <div class="x-col" xid="col9" style="padding:0px;">
          <a component="$UI/system/components/justep/button/button" class="btn btn-link btn-xs btn-only-icon" label="button" xid="button2" icon="icon-ios7-telephone" style="font-size:24px;">
           <i xid="i3" class="icon-ios7-telephone"></i>
           <span xid="span5"></span></a> </div> 
         <div class="x-col" xid="col10" style="padding:0px;">
          <a component="$UI/system/components/justep/button/button" class="btn btn-link btn-xs btn-only-icon" label="button" xid="button3" icon="icon-social-designernews" style="font-size:24px;" onClick="button3Click">
           <i xid="i4" class="icon-social-designernews"></i>
           <span xid="span6"></span></a> </div> </div> </div> </div> </div> </div> </div> </div></div>  
        <div class="x-panel-content" xid="content2" style="background:#fff;margin-top:124px;">
        
        <div component="$UI/system/components/justep/row/row" class="x-row x-row-center" xid="row2" style="text-align:center;background:#fffddd;height:40px;padding:0px 0px 0px 0px;line-height:40px;border-bottom: 1px solid #ffe5b6;">
   <div class="x-col x-col-center" xid="col14">
    <span xid="span8"><![CDATA[介绍]]></span></div> </div>
  <div component="$UI/system/components/justep/row/row" class="x-row" xid="row9">
   <div class="x-col" xid="col17"><div class="" style="padding:10px" bind-text=' $model.zhuanjia.val("xiang")' xid="div1"></div></div></div>
  </div> 
      </div>
    </div> 
  </div> 
</div>
