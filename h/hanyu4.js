define(function(require) {
	var $ = require("jquery");
	var justep = require("$UI/system/lib/justep");

	var Model = function() {
		this.callParent();
	};
	
	//图片路径转换
	Model.prototype.getImageUrl = function(url){
		return require.toUrl(url);
	};
	
	//获取一级分类信息
	/*
	1、默认显示当前一级菜单对应的二、三级数据
	2、点击其它一级菜单，再加载它的二三级数据
	*/
	Model.prototype.rootClassDataCustomRefresh = function(event){		
		/*
		1、加载一级分类数据
		 */
		var url = require.toUrl("./class/json/rootClassData.json");
		var rootClassData = event.source;
		rootClassData.clear();
		$.ajaxSettings.async = false;
		$.getJSON(url, function(data) {
			rootClassData.loadData(data);
		});      
	};

	Model.prototype.shujuCustomRefresh = function(event){
		var shuju = this.comp("shuju");
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/getStudyArticle',
			async: false,
			cache: false,
			success:function(data){
				for(var i in data){
					shuju.newData({
						"defaultValues" : [ {
							"fID": data[i].id ,
							"fenleiid": 2,
							"img":window.imgshou+data[i].coverPhoto,
							"name": data[i].title,
							"yueduliang": data[i].clicks
						}]
					});
				}
				
			},
			error:function(e){
			
			}
		})
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/getStudyVideo',
			async: false,
			cache: false,
			success:function(data){
				for(var i in data){
					shuju.newData({
						"defaultValues" : [{
							"fID": data[i].id ,
							"fenleiid": 1,
							"img":window.imgshou+data[i].coverPhoto,
							"name": data[i].title,
							"yueduliang": data[i].clicks,
							"content":data[i].content,
							"video": window.imgshou+data[i].videoUrl
						}]
					});
				}
			},
			error:function(e){
			
			}
		})
		
		$.ajax({
			type:"GET",
			url:window.yuming+'/app/getTask',
			async: false,
			cache: false,
			success:function(data){
				for(var i in data){
					shuju.newData({
						"defaultValues" : [ {
							"fID": data[i].id ,
							"fenleiid": 4,
							"img":'images/924da128d58d1979!600x600.jpg',
							"name": data[i].title,
							"content":data[i].content,
							"diagnose":data[i].processingMethod,
						}]
					});
				}
			},
			error:function(e){
			
			}
		})
	};
	
	/*//获取二级分类信息	
	Model.prototype.secondClassDataCustomRefresh = function(event){
		
		1、加载二级分类数据
		 
		var url = require.toUrl("./class/json/secondClassData.json");
		var secondClassData = event.source;
		secondClassData.clear();
		$.ajaxSettings.async = false;
		$.getJSON(url, function(data) {
			secondClassData.loadData(data);
		});
	};
	//获取三级分类信息
	Model.prototype.threeClassDataCustomRefresh = function(event){
		
		1、加载三级分类数据
		 
		var url = require.toUrl("./class/json/threeClassData.json");
		var threeClassData = event.source;
		threeClassData.clear();
		$.ajaxSettings.async = false;
		$.getJSON(url, function(data) {
			threeClassData.loadData(data);
		});
	};*/
	
	//商品点击事件
	Model.prototype.listClick = function(event){
		/*
		 1、获取当前商品ID
		 2、转到商品详细信息页，带参数
		 3、在详细页的接收事件中，从服务端过滤数据

		 显示页面代码如下：
		 justep.Shell.showPage("list",{
			keyValue : this.comp("threeClassData").getValue("fClassName")
		 });
		 */
	};
	Model.prototype.li2Click = function(event){
		var row = event.bindingContext.$object.toJson();
		var classify_id = row.fenleiid.value;
		if(classify_id == 1){
			this.comp("video").open({
		        params:{
		              rowdata:row
		        }
	        });  
		}else if(classify_id == 2){
			this.comp("img1").open({
		        params:{
		              rowdata:row
		        }
	        }); 
		}else if(classify_id == 3){
			this.comp("live1").open({
		        params:{
		              rowdata:row
		        }
	        }); 
		}else{
			this.comp("cases").open({
		        params:{
		              rowdata:row
		        }
	        }); 
		}
	};
	return Model;
});