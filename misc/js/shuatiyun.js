/*$(function(){
	 alert(123)
	 TouchPoint.color = 'red';
	  TouchPoint.init(); 
})*/

function shapeNode(rt) {
    if (document.createTreeWalker) {
        var tw = document.createTreeWalker(rt, NodeFilter.SHOW_TEXT, null, false);
        var a = 0;
        while (tw.nextNode()) {
            tw.currentNode.data = Unicode2M(tw.currentNode.data);
        }
    } else {
        var n = rt.childNodes[0];
        while (n != null) {
            if (n.nodeType == 3) {
                n.nodeValue = Unicode2M(n.nodeValue);
            }
            if (n.hasChildNodes()) {
                n = n.firstChild;
            } else {
                while (n != null && n.nextSibling == null && n != rt) {
                    n = n.parentNode;
                }
                if (n != null) n = n.nextSibling;
            }
        }
    };
    var t = document.querySelectorAll('input[type=button], input[type=submit], input[type=reset],input[type=text],input[type=search]');
    for (var i = 0; i < t.length; i++) {
        t[i].value = t[i].value == "" ? "" : Unicode2M(t[i].value);
    }
};
